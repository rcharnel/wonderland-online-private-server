﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net.Sockets;
using pserver.Network;

namespace pserver
{
    #region DLogger Delegates

    public delegate void onLogEvent();
    #endregion

    #region Wlo Client Delegates
    public delegate void onNetClientConnect(Socket rawSock);
    public delegate void onPacketRecieve(RecvPacket pkt ,Player src);
    public delegate void onPacketSend(SendPacket pkt);
    #endregion
}
