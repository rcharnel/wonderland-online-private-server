﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using pserver.Network;

namespace pserver
{
    public class StarterShip : MapPlugin
    {
        //BattleManager gBattleManager { get; }
        public override ushort MapID { get { return 10017; } }
        public override string Name { get { return "StarterShip"; } }

        public override void onLogin(ref Player player)
        {
            for (int a = 0; a < Players.Count; a++)
            {
                if (Players[a] == player) return;
            }
            player.Info.currentMap = this;
            base.onLogin(ref player);

            for (int a = 0; a < Players.Count; a++)
            {
                //send to them
                player.Send_3_They(Players[a]);
                SendPacket p = new SendPacket();
                p.Header(5, 0);
                p.AddDWord(player.CharacterID);
                p.AddArray(player.Eqs.Worn_Equips);
                Players[a].Send(p);
                p = new SendPacket();
                p.Header(10, 3);
                p.AddDWord(player.CharacterID);
                p.AddByte(255);
                Players[a].Send(p);//maybe guild info???
                p = new SendPacket();
                p.Header(5, 8);
                p.AddDWord(player.CharacterID);
                p.AddByte(0);
                Players[a].Send(p);

                //send to me
                p = new SendPacket();
                p.Header(7);
                p.AddDWord(Players[a].CharacterID);
                p.AddWord(MapID);
                p.AddWord(Players[a].X_Axis);
                p.AddWord(Players[a].Y_Axis);
                player.Send(p);
                p = new SendPacket();
                p.Header(5, 0);
                p.AddDWord(Players[a].CharacterID);
                p.AddArray(Players[a].Eqs.Worn_Equips);
                player.Send(p);
            }

            Players.Add(player);
            SendMapInfo(player);
            //Send_32_2(c);
        }

        
        public override void onTerminated()
        {
            isRemoved = true;
        }

        public override void onLoad()
        {
            isRemoved = false;
            base.onLoad();
        }


        public override void UpdateMap()
        {
        }
    }
}
