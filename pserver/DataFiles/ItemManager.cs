﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace pserver
{
    public class ItemManager
    {
        Dictionary<ushort, cItem> itemList = new Dictionary<ushort, cItem>();

        public ItemManager()
        {

        }

        public cItem GetItem(ushort id)
        {
            try { return itemList[id]; }
            catch { return null; }
        }

        public bool LoadItems(string path)
        {
            try
            {
                itemList.Clear();
                if (!File.Exists(path)) return false;
                DLogger.SystemLog("Loading Item.Dat");
                byte[] data = File.ReadAllBytes(path);
                int max = data.Length / 457;
                int at = 0;
                for (int n = 0; n < max; n++)
                {
                    cItem i = new cItem();
                    i.Ammt = 1;
                    i.Data.mydata = new byte[457];
                    Array.Copy(data, at, i.Data.mydata, 0, 457);
                    i.Data.Load();
                    itemList.Add(i.ItemID, i);
                    at += 457;
                }
                DLogger.SystemLog("Item.Dat Loaded ( "+itemList.Count+" Items)");
                return true;
            }
            catch (Exception e) { DLogger.ErrorLog(e.StackTrace, e.Message); }
            itemList.Clear();
            DLogger.SystemLog("Item.dat not Loaded");
            return false;
        }
    }
}
