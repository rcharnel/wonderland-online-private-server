﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using System.Windows.Forms;
using System.Threading;
using System.IO;
using pserver.Network;
using System.Reflection;
using pserver.Ac;

namespace pserver
{
    public partial class Form1 : Form
    {
        Thread BGTask;

        #region Time Checks
        DateTime hrcheck;
        DateTime serv_Status_2min;
        DateTime serv_Status_1min;
        #endregion
        #region Listbox
        DateTime autoscrolldelay;
        bool autoscrolling;
        #endregion
        
        public Form1()
        {
            InitializeComponent();
            dataGridView1.DataSource = DLogger.LogList;
            DLogger.FrmSrc = this;
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            Directory.CreateDirectory("Logs\\System\\");
            Directory.CreateDirectory("Logs\\DataBase\\");
            Directory.CreateDirectory("Logs\\NetWork\\");
            Directory.CreateDirectory("Logs\\Error\\");
            using (StreamWriter t = new StreamWriter("Logs\\System\\"+DateTime.Now.ToShortDateString().Replace("/", "") + "Log.txt"))
            {
                t.WriteLine("Server Started\r\n===");
            }
            BGTask = new System.Threading.Thread(new ThreadStart(BGTask_DoWork));
            BGTask.IsBackground = true;
            BGTask.Start();
        }

        void BGTask_DoWork()
        {
            Thread.Sleep(2000);

            #region Loading
            // Setting Up Objects    
            DLogger.LogList.ListChanged += LogList_ListChanged;
            DLogger.SystemLog("Setting Up Objects");

            cGlobal.TestDB = new DataBase.Admin_DataBase();
            cGlobal.WLO4EVERDB = new DataBase.WLO4EVER_Database();
            cGlobal.CharacterDB = new DataBase.gCharacterDataBase();
            cGlobal.GameDB = new DataBase.GameDatabase();
            cGlobal.ServerDB = new DataBase.SYSDatabase();
            cGlobal.ServerSettings = new Gui.Settings();
            cGlobal.ThreadManager = new Templates.gThreadManager();
            

            cGlobal.WloItemManager = new ItemManager();

            cGlobal.WlOTcpListener = new Network._6414Listener();
            cGlobal.LoginSrv = new Server();
            cGlobal.World = new Templates.MapManager();
            cGlobal.PacketList = new Queue<WloPacket>();
            cGlobal.Aclist = new Dictionary<int, AC>();


            DLogger.SystemLog("Loading AC's");
            foreach (var t in (from c in Assembly.GetExecutingAssembly().GetTypes()
                               where c.IsClass && c.IsSubclassOf(typeof(AC))
                               select c))
            {
                try
                {
                    var tp = (Activator.CreateInstance(t) as AC);
                    cGlobal.Aclist.Add(tp.ID, tp);
                }
                catch { }
            }

            DLogger.SystemLog("Loaded " + cGlobal.Aclist.Count + " ActionCode Code Structures");


            DLogger.SystemLog("Loading WLO Data Files");
            cGlobal.WloItemManager.LoadItems("data\\Item.Dat");


            DLogger.SystemLog("Testing DataBase Connections");
            //We need all 3 active to Proceed
            if (!((cGlobal.WLO4EVERDB.Connect() || cGlobal.TestDB.Connect()) && cGlobal.CharacterDB.Connect() && cGlobal.GameDB.Connect()))
            {
                if (MessageBox.Show("Activate Dev Mode?\r\n No access to DB and will have default Character", "", MessageBoxButtons.YesNo) == System.Windows.Forms.DialogResult.No)
                {
                    MessageBox.Show("DataBase Server Down.. Unable to Proceed\r\nServer Shutting Down!", "Critical Requirement");
                    this.Invoke(new Action(() => { Close(); }));
                }
                else
                {
                }
                cGlobal.DEVMODE = true;
            }

            #endregion

            DLogger.SystemLog("Setting Up TCP NetWork");
            cGlobal.WlOTcpListener.ClientConnected += cGlobal.LoginSrv.WlOTcpListener_ClientConnected;
            cGlobal.WlOTcpListener.Start();
            DLogger.SystemLog("Starting Primary Packet Thread");
            cGlobal.ThreadManager.Threads++;

            while (!cGlobal.SHUTDOWN)
            {

                #region Maintain 100 Logging info
                if (DLogger.LogList.Count > 100)
                {
                    var y = DLogger.PullFirstLog();
                    string dir = "";
                    switch (y.Type)
                    {
                        case LogType.System: dir = "Logs\\System\\"; break;
                        case LogType.DataBase: dir = "Logs\\DataBase\\"; break;
                        case LogType.Error: dir = "Logs\\Error\\"; break;
                        case LogType.NetWork: dir = "Logs\\NetWork\\"; break;
                    }

                    try
                    {
                        using (StreamWriter t = new StreamWriter(dir + DateTime.Now.ToShortDateString().Replace("/", "") + "Log.txt", true))
                            t.WriteLine(string.Format("{0} -- {1} {2}\r\n", y.When.ToString("hh:mm:ss"), y.Message, y.where));
                    }
                    catch { }
                }

                #endregion
                #region Update Time every Hr
                if (hrcheck == null || hrcheck.Hour < DateTime.Now.Hour)
                {
                    hrcheck = DateTime.Now;
                    SendPacket tmp = new SendPacket();
                    tmp.Header(23, 140);
                    tmp.AddByte(3);
                    tmp.AddDouble(DateTime.Now.ToOADate());
                    cGlobal.PlayerList.ToList().ForEach(new Action<Player>(c => c.Send(tmp)));
                }
                #endregion
                #region Update Maps
                cGlobal.World.UpdateALL();
                #endregion
                #region Manage Thread
                int pktcnt = 0;
                try { pktcnt = (int)Math.Round((decimal)( cGlobal.PacketList.Count / 100)); }
                catch { }
                if (pktcnt > cGlobal.ThreadManager.Threads)
                    cGlobal.ThreadManager.Threads++;
                else if (cGlobal.PacketList.Count < 50 && cGlobal.ThreadManager.Threads > 2)
                    cGlobal.ThreadManager.Threads--;
                #endregion
                #region Manage InActivePlayers
                foreach (var p in cGlobal.PlayerList.ToList())
                    if (!p.isConnected) p.Disconnect(true);
                #endregion

                if (!cGlobal.DEVMODE)
                {
                    #region Manage DataBase Stats
                    // Divide the info based on time to refresh

                    if (serv_Status_2min == null || serv_Status_2min < DateTime.Now) //every 2mins might change later
                    {
                        serv_Status_2min = DateTime.Now.AddMinutes(2);
                        cGlobal.ServerDB.Update_Exp_Drop_Rate(1, cGlobal.ServerSettings.EXP_Rate, cGlobal.ServerSettings.DROP_Rate);
                    }
                    else if (serv_Status_1min == null || serv_Status_1min < DateTime.Now) //every 1 min
                    {
                        serv_Status_1min = DateTime.Now.AddMinutes(1);
                        cGlobal.ServerDB.UpdatePlayerCnt(1, cGlobal.PlayerList.Count);
                    }
                    #endregion
                }

                Thread.Sleep(1);
            }
        }
               
        #region Forum Stuff
        void LogList_ListChanged(object sender, ListChangedEventArgs e)
        {
            if(autoscrolldelay == null || autoscrolldelay < DateTime.Now)
                switch (e.ListChangedType)
                {
                    case ListChangedType.ItemAdded:
                    case ListChangedType.ItemDeleted:
                    case ListChangedType.Reset:{autoscrolling = true; try { this.Invoke(new Action(() => { try { dataGridView1.FirstDisplayedScrollingRowIndex = dataGridView1.RowCount - 1; } catch { } })); }
                        catch { } autoscrolling = false;}break;
                }
        }

        private void dataGridView1_RowPrePaint(object sender, DataGridViewRowPrePaintEventArgs e)
        {
            dataGridView1.ClearSelection();
            if (DLogger.LogList[e.RowIndex].Type == LogType.System)
                dataGridView1.Rows[e.RowIndex].DefaultCellStyle.ForeColor = Color.Blue;
            else if (DLogger.LogList[e.RowIndex].Type == LogType.DataBase)
                dataGridView1.Rows[e.RowIndex].DefaultCellStyle.ForeColor = Color.DarkGreen;
            else if (DLogger.LogList[e.RowIndex].Type == LogType.Error)
                dataGridView1.Rows[e.RowIndex].DefaultCellStyle.ForeColor = Color.Red;
        }

        private void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            dataGridView1.ClearSelection();
        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            
            MessageBox.Show("Please wait Shutting Down, Ejecting Players and Saving Info/Settings");
            cGlobal.LoginSrv.ShutDown();
            cGlobal.PlayerList.ToList().ForEach(new Action<Player>(c => c.Disconnect(true)));
            while (cGlobal.PlayerList.Count > 0) { Thread.Sleep(10); }
            cGlobal.SHUTDOWN = true;
            cGlobal.ThreadManager.Shutdown();
            while (cGlobal.ThreadManager.Threads > 0) { Thread.Sleep(100); }
            cGlobal.WlOTcpListener.Stop();
            
            this.Invoke(new Action(() => { while (BGTask.IsAlive) { Thread.Sleep(3000); BGTask.Abort(); } }));

            while (DLogger.LogList.Count > 0)
            {
                var y = DLogger.PullFirstLog();
                string dir = "";
                switch (y.Type)
                {
                    case LogType.System: dir = "Logs\\System\\"; break;
                    case LogType.DataBase: dir = "Logs\\DataBase\\"; break;
                    case LogType.Error: dir = "Logs\\Error\\"; break;
                    case LogType.NetWork: dir = "Logs\\NetWork\\"; break;
                }

            retry:
                try
                {
                    using (StreamWriter t = new StreamWriter(dir + DateTime.Now.ToShortDateString().Replace("/", "") + "Log.txt", true))
                        t.WriteLine(string.Format("{0} -- {1} {2}\r\n", y.When.ToString("hh:mm:ss"), y.Message, y.where));
                }
                catch { goto retry; }
            }
        }
        
        private void dataGridView1_Scroll(object sender, ScrollEventArgs e)
        {
            if (!autoscrolling)
                autoscrolldelay = DateTime.Now.AddSeconds(45);
              

        }
        #endregion
              
    }
    public struct WloPacket
    {
        public Player src;
        public RecvPacket packet;
    }
}
