﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using pserver.Gui;
using DataBase;
using pserver.Network;
using System.ComponentModel;
using pserver.Templates;
using pserver.Ac;

namespace pserver
{
    /// <summary>
    /// Global Class for the entire Server
    /// </summary>
    public static class cGlobal
    {
        public static Queue<WloPacket> PacketList;
        
        public static Dictionary<int, AC> Aclist;

        public static  BindingList<Player> PlayerList { get { return World.PlayerList ?? new BindingList<Player>(); } }

        public static volatile bool DEVMODE;

        public static volatile bool SHUTDOWN;

        public static MapManager World;

        public static Server LoginSrv;

        public static gThreadManager ThreadManager;

        /// <summary>
        /// User DataBase of Wlo 4 Ever .com
        /// </summary>
        public static WLO4EVER_Database WLO4EVERDB;
        /// <summary>
        /// UserDataBase of Test DB
        /// </summary>
        public static Admin_DataBase TestDB;
        /// <summary>
        /// Server Character DataBase;
        /// </summary>
        public static gCharacterDataBase CharacterDB;
        /// <summary>
        /// Server Game DataBase;
        /// </summary>
        public static GameDatabase GameDB;
        /// <summary>
        /// Server Status DataBase Made to avoid issues later on
        /// </summary>
        public static SYSDatabase ServerDB;
        /// <summary>
        /// Global Settings of Server
        /// </summary>
        public static Settings ServerSettings;
        /// <summary>
        /// All of Wlo Item's
        /// </summary>
        public static ItemManager WloItemManager;
        /// <summary>
        /// Wlo Tcp Listener
        /// </summary>
        public static Network._6414Listener WlOTcpListener;



        #region Map related Function
        public static void Teleport(ref Player sender, byte portalID,bool byTool = false)
        {
            WarpPortal dst = null;
            if (sender.CurrentMap.Teleport(ref sender, portalID,out dst))
            {
                SendPacket tmp = new SendPacket();
                if (!byTool)
                {
                    tmp.Header(20, 7);
                    sender.Send(tmp);
                }
                tmp = new SendPacket();
                tmp.Header(23, 32);
                tmp.AddDWord(sender.CharacterID);
                sender.Send(tmp);
                tmp = new SendPacket();
                tmp.Header(23, 112);
                tmp.AddDWord(sender.CharacterID);
                sender.Send(tmp);
                tmp = new SendPacket();
                tmp.Header(23, 132);
                tmp.AddDWord(sender.CharacterID);
                sender.Send(tmp);
                sender.CurrentMap.onWarp_Out(ref sender, dst);

                ushort prevmap,prevx,prevy;
                prevmap = sender.CurrentMap.MapID;
                prevx=(ushort)(sender.X_Axis-20);
                prevy=(ushort)(sender.Y_Axis+20);
                sender.Info.currentMap = null;

                //Transfer to New Map
                if (World.Player_Teleport(dst, ref sender))
                {
                    sender.DataOut = SendType.Queue;
                    sender.CurrentMap.SendMapInfo(sender);
                    sender.DataOut = SendType.Normal;
                }
                else
                {
                    dst.DstMap = prevmap;
                    dst.DstX_Axis = prevx;
                    dst.DstY_Axis = prevy;
                    World.Player_Teleport(dst, ref sender);
                }


            }
            else
            {
                SendPacket tmp = new SendPacket();
                tmp.Header(20, 8);
                sender.Send(tmp);
            }
        }

        #endregion


        #region Generic Conversion Methods
        public static int MatrixtoNumber(int a, int b) { return ((a * 5) + (b - 5)); }//wlo specific
        public static byte[] NumbertoMatrix(int a)
        {
            var s = 0;
            if (a == 5 || a == 10 || a == 15 || a == 20 || a == 25 || a == 30 || a == 35 || a == 40 || a == 45 || a == 50)
                s = (a / 5);
            else
                s = 1 + (a / 5);
            var t = 0;
            if (a == 5 || a == 10 || a == 15 || a == 20 || a == 25 || a == 30 || a == 35 || a == 40 || a == 45 || a == 50)
                t = 5;
            else if (a > 5)
                t = 5 - (((1 + (a / 5)) * 5) - a);
            else
                t = a;
            byte[] matrixloc = new byte[2];
            matrixloc[0] = (byte)(s);
            matrixloc[1] = (byte)(t);
            return matrixloc;
        }//wlo specific


        public static byte[] ToStringArray(string str)
        {
            List<byte> data = new List<byte>();
            data.Add((byte)str.Length);
            for (int n = 0; n < str.Length; n++)
                data.Add((byte)str[n]);
            return data.ToArray();
        }
        public static byte[] ToDWordArray(UInt32 v)
        {
            List<byte> data = new List<byte>();
            data.Add((byte)v);
            data.Add((byte)(v >> 8));
            data.Add((byte)(v >> 16));
            data.Add((byte)(v >> 24));
            return data.ToArray();
        }

        public static void InsertWord(UInt16 value, ref byte[] data, uint at)
        {
            data[at] = (byte)value;
            data[at + 1] = (byte)(value >> 8);
        }
        public static void InsertDWord(UInt32 value, ref byte[] data, uint at)
        {
            data[at] = (byte)value;
            data[at + 1] = (byte)(value >> 8);
            data[at + 2] = (byte)(value >> 16);
            data[at + 3] = (byte)(value >> 24);
        }


        public static string PullString(uint at, byte[] data)
        {
            int len = data[at];
            char[] c = new char[len];
            Array.Copy(data, at + 1, c, 0, len);
            string str = new string(c);
            return str;
        }
        public static UInt32 PullDWord(uint at, byte[] data)
        {
            UInt32 v = (UInt32)(data[at] + (data[at + 1] << 8) + (data[at + 2] << 16) + (data[at + 3] << 24));
            return v;
        }
        public static UInt16 PullWord(uint at, byte[] data)
        {
            UInt16 v = (UInt16)(data[at] + (data[at + 1] << 8));
            return v;
        }
        #endregion
    }
}
