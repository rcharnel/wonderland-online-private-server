﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace pserver
{  

    public enum SendType
    {
        Normal,
        Multi,
        Queue,
    }
    public enum ElementType
    {
        Normal,
        Earth,
        Water,
        Fire,
        Wind,
    }

    
    public enum ChatType
    {
        Local,
        World,
        Whisper,
        Guild,
        Team,
    }
    
    public enum inGameState
    {
        Normal,
        Sitting,
        Emote,
        Battle,
        RidingNpc,
        RidingVech,
    }

    #region Character Enums
    
    public enum RebornJob : byte
    {
        none,
        Killer = 1,
        Warrior,
        Knight,
        Wit,
        Priest,
        Seer,
    }

    public enum BodyStyle
    {
        none,
        Small_Male = 1,
        Small_Female,
        Big_Male,
        Big_Female,
    }

    public enum HairStyle_SmallM
    {
        Rocco,
    }
    public enum HairStyle_SmallF
    {
        Nina,
        Betty,
    }
    public enum HairStyle_BigM
    {
        Daniel,
        Sid,
        More,
        Kurogane,
    }
    public enum HairStyle_BigF
    {
        Iris,
        Lique,
        Vanessa,
        Breillat,
        Jessica,
        Konnotsuroko,
        Maria,
        Karin,
    }
    #endregion

    #region Map Related
    public enum WarpType
    {
        LoginWarp,
        regularWarp,
        Questwarp,
        ToolWarp,
        SpecialWarp,
    }
    public enum MapState
    {
        Initialized = 0,
        Dead,
        HasCharacterTemplates,
        Idle,
    }
    public enum EmoteType
    {
        Sitting = 51,
        Standing = 13,
        LayingDown = 27,
    }
    public enum MapType
    {
        None,
        RegularMap,
        Tent,
        Carnie,
        Instance,
        Dungen,
        DungenSafeZone
    }
    #endregion

    #region Item Related
    public enum eWearSlot
    {
        none = 0,
        head = 1,
        body = 2,
        hand = 3,
        arm = 4,
        feet = 5,
        special = 6

    }
    public enum eItemType
    {
        unknown =-1,
        none = 0,
        single_edge = 1,
        sword = 2,
        spear = 3,
        bow = 4,
        wand = 5,
        claw = 6,
        axe = 8,
        pestle = 9,
        Fan = 10,
        Gun = 11,
        bodyarmor = 12,
        headwear = 13,
        armitem = 14,
        footwear = 15,

    }

    public enum eSpecialStatus
    {
        Monster_Records = 101,
        Critical_Increase = 137,
    }
    #endregion


    #region Battle Related
    public enum BattleState
    {
        Battle = 0,
        Rest = 1,
        Ride = 2,

    }
    public enum eBattleState
    {
        notActive,
        Active,
        Ended,
    }
    public enum eBattleType
    {
        pk = 2,
        normal,
        quest,
    }
    public enum eBattleRoundState
    {
        none,
        PrepState,
        ReadyState,
        CalculatingState,
        EndedState,
    }
    public enum eBattleLeaveType
    {
        BattleFinished,
        RunAway,
        Spawn,
        Dced,
    }
    public enum UnknownType
    {
        miss = 2,
    }
    public enum Side
    {
        none,
        left = 2,
        right = 5,
        watch = 4,
    }
    public enum eFighterType
    {
        none = 0,
        player = 2,
        Npc = 7,
        Watcher = 2,
        Pet = 4,
    }
    public enum eFighterState
    {
        Alive,
        Sealed,
        Dead,
    }

    #endregion

    #region Pet
    public enum Tradeable
    {
        Yes = 0,
        No = 1,
    }
    public enum TransferPet
    {
        yes = 0,
        no = 1,
    }
    public enum NPCType
    {
    }
    public enum eAC8_2_Skill
    {
        CHP = 25,
        CSP = 26,
        DEF = 42,
        MATK = 43,
        MDEF = 44,
        SPD = 45,
        ATK = 46,
        v207 = 207,
        v208 = 208,
        v210 = 210,
        v211 = 211,
        v214 = 214,
        v215 = 215,
        v216 = 216,
    }
    public enum PetStatus
    {
        Stored,
        Resting,
        RidingPet,
        Battle,
    }

    #endregion


    #region Quest Related
    public enum QuestType
    {
    }
    #endregion

    #region Event Related
    public enum EventType
    {
        Sitting,
        LayingDown,
    }
    #endregion

    #region AI
    public enum AIBrainType
    {
        Guard,
    }

    #endregion

    #region threads

    public enum ThreadType
    {
        Recv,
        Update,
    }
    #endregion

    #region Dungen




    #endregion
}
