﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Security.Cryptography;
using System.IO;

namespace pserver
{
    public static class HelperExt
    {
        #region Security
        public static readonly HashAlgorithm MD5 = new MD5CryptoServiceProvider();
        public static readonly HashAlgorithm SHA1 = new SHA1Managed();
        public static readonly HashAlgorithm SHA256 = new SHA256Managed();
        public static readonly HashAlgorithm SHA384 = new SHA384Managed();
        public static readonly HashAlgorithm SHA512 = new SHA512Managed();
        public static readonly HashAlgorithm RIPEMD160 = new RIPEMD160Managed();
        #endregion
        #region Images
        //public static byte[] imageToByteArray(BitmapImage bitmapImage)
        //{

        //    BmpBitmapEncoder encoder = new BmpBitmapEncoder();
        //    encoder.Frames.Add(BitmapFrame.Create(bitmapImage));
        //    using (MemoryStream ms = new MemoryStream())
        //    {
        //        encoder.Save(ms);
        //        return ms.ToArray();
        //    }
        //}
        //public static BitmapImage byteArrayToImage(byte[] imageData)
        //{
        //    if (imageData == null || imageData.Length == 0) return null;
        //    var image = new BitmapImage();
        //    using (var mem = new MemoryStream(imageData))
        //    {
        //        mem.Position = 0;
        //        image.BeginInit();
        //        image.CreateOptions = BitmapCreateOptions.PreservePixelFormat;
        //        image.CacheOption = BitmapCacheOption.OnLoad;
        //        image.UriSource = null;
        //        image.StreamSource = mem;
        //        image.EndInit();
        //    }
        //    image.Freeze();
        //    return image;
        //}
        #endregion
        #region File and System.IO Related Functions
        public static string ProgramFilesx86()
        {
            if (8 == IntPtr.Size
                || (!String.IsNullOrEmpty(Environment.GetEnvironmentVariable("PROCESSOR_ARCHITEW6432"))))
            {
                return Environment.GetEnvironmentVariable("ProgramFiles(x86)");
            }

            return Environment.GetEnvironmentVariable("ProgramFiles");
        }

        public static bool IsFileInUse(string fileFullPath, bool throwIfNotExists)
        {
            if (System.IO.File.Exists(fileFullPath))
            {
                try
                {
                    //if this does not throw exception then the file is not use by another program
                    using (FileStream fileStream = File.OpenWrite(fileFullPath))
                    {
                        if (fileStream == null)
                            return true;
                    }
                    return false;
                }
                catch
                {
                    return true;
                }
            }
            else if (!throwIfNotExists)
                return true;
            else
                throw new FileNotFoundException("Specified path is not exsists", fileFullPath);
        }
        #endregion
        #region Conversion
        public static DateTime ConverttoServerTime(DateTime obj, TimeZoneInfo dst)
        {
            return TimeZoneInfo.ConvertTime(obj, TimeZoneInfo.Local, dst);
        }
        public static DateTime ConverttoMyTime(DateTime obj, TimeZoneInfo src)
        {
            return TimeZoneInfo.ConvertTime(obj, src, TimeZoneInfo.Local);
        }


        public static int MatrixtoNumber(int a, int b) { return ((a * 5) + (b - 5)); }
        public static byte[] NumbertoMatrix(int a)
        {
            var s = 0;
            if (a == 5 || a == 10 || a == 15 || a == 20 || a == 25 || a == 30 || a == 35 || a == 40 || a == 45 || a == 50)
                s = (a / 5);
            else
                s = 1 + (a / 5);
            var t = 0;
            if (a == 5 || a == 10 || a == 15 || a == 20 || a == 25 || a == 30 || a == 35 || a == 40 || a == 45 || a == 50)
                t = 5;
            else if (a > 5)
                t = 5 - (((1 + (a / 5)) * 5) - a);
            else
                t = a;
            byte[] matrixloc = new byte[2];
            matrixloc[0] = (byte)(s);
            matrixloc[1] = (byte)(t);
            return matrixloc;
        }


        public static byte[] ToStringArray(string str)
        {
            List<byte> data = new List<byte>();
            data.Add((byte)str.Length);
            for (int n = 0; n < str.Length; n++)
                data.Add((byte)str[n]);
            return data.ToArray();
        }
        public static byte[] ToDWordArray(UInt32 v)
        {
            List<byte> data = new List<byte>();
            data.Add((byte)v);
            data.Add((byte)(v >> 8));
            data.Add((byte)(v >> 16));
            data.Add((byte)(v >> 24));
            return data.ToArray();
        }

        public static void InsertWord(UInt16 value, ref byte[] data, uint at)
        {
            data[at] = (byte)value;
            data[at + 1] = (byte)(value >> 8);
        }
        public static void InsertDWord(UInt32 value, ref byte[] data, uint at)
        {
            data[at] = (byte)value;
            data[at + 1] = (byte)(value >> 8);
            data[at + 2] = (byte)(value >> 16);
            data[at + 3] = (byte)(value >> 24);
        }


        public static string PullString(uint at, byte[] data)
        {
            int len = data[at];
            char[] c = new char[len];
            Array.Copy(data, at + 1, c, 0, len);
            string str = new string(c);
            return str;
        }
        public static UInt32 PullDWord(uint at, byte[] data)
        {
            UInt32 v = (UInt32)(data[at] + (data[at + 1] << 8) + (data[at + 2] << 16) + (data[at + 3] << 24));
            return v;
        }
        public static UInt16 PullWord(uint at, byte[] data)
        {
            UInt16 v = (UInt16)(data[at] + (data[at + 1] << 8));
            return v;
        }

        #endregion

        public static void Sleep(int timeout_miliseconds)
        {
            System.Threading.Thread.Sleep(timeout_miliseconds);
        }
    }
}
