﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using System.Reflection;
using pserver.Network;

namespace pserver.Templates
{
    public class gThread
    {
        public event EventHandler Done;
        bool cancel = false;
        public bool PendingCancel;
        public Thread worker { get; set; }

        public gThread()
        {
            worker = new Thread(new ThreadStart(worker_DoWork));
            worker.IsBackground = true;
            worker.Start();
        }

        public void Terminate()
        {
            cancel = PendingCancel = true;

            if (worker.IsAlive) worker.Abort();
                
                if (Done != null) Done(this, null);
        }

        void worker_DoWork()
        {
            while (!cancel)
            {
                #region Receiving Packets
                if (cGlobal.PacketList.Count > 0)
                {
                    var pkt = cGlobal.PacketList.Dequeue();
                    try
                    {

                        if (pkt.src.isConnected)
                        {
                            if (cGlobal.Aclist.ContainsKey(pkt.packet.a))
                                try { cGlobal.Aclist[(int)pkt.packet.a].Switchboad(ref pkt.src, pkt.packet); }
                                catch (Exception t) { DLogger.ErrorLog(t.StackTrace, t.Message); }
                            else
                                DLogger.NetworkLog(pkt.packet.a, null, "is Not Coded");
                        }
                    }
                    catch (Exception r)
                    {
                        DLogger.ErrorLog(r.StackTrace, r.Message);
                        SendPacket p = new SendPacket();
                        p.Header(0, 55);
                        try { pkt.src.Send(p); }
                        catch { }
                    }
                }

                #endregion

                Thread.Sleep(2);
            }
            if (Done != null) Done(this, null);
        }
    }

    public class gThreadManager
    {
        List<gThread> tlist = new List<gThread>();
        
        
        public int Threads
        {
            get { return tlist.Count; }
            set
            {
                if (tlist.Count > value && !tlist[0].PendingCancel)
                    tlist[0].Terminate();
                else if (tlist.Count < value)
                {
                    gThread tmp = new gThread();
                    tmp.Done += tmp_Done;
                    tlist.Add(tmp);

                }

            }
        }

        void tmp_Done(object sender, EventArgs e)
        {
            tlist.RemoveAt(0);
        }

        public void Shutdown()
        {
            foreach (var e in tlist.ToList())
                e.Terminate();
        }
    }
}
