﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace pserver
{
    public class cItem
    {
        public cItem()
        {
            Clear();
        }

        ItemData data; public ItemData Data { get { return data ?? new ItemData(); } private set { data = value; } }
        public UInt16 ItemID { get { try { return Data.ItemID; } catch { return 0; } } }
        public bool Stackable { get { try { return Data.able_to_stack; } catch { return false; } } }
        public byte Ammt;
        public byte Damage;
        public byte ParentSlot;
        public bool locked;

        public int InvHeight { get { try { return Data.InvHeight; } catch { return 0; } } }
        public int InvWidth { get { try { return Data.InvWidth; } catch { return 0; } } }

        public void Clear()
        {
            Data = new ItemData();
            Ammt = 0;
            Damage = 0;
            ParentSlot = 0;
            locked = false;
        }

        public void CopyFrom(cItem i)
        {
            if (i != null)
            {
                Data = i.Data;
                Ammt = i.Ammt;
                Damage = i.Damage;
                ParentSlot = i.ParentSlot;
                locked = i.locked;
            }
        }
    }
}
