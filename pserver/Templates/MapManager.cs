﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;
using pserver.Network;
using System.Reflection;

namespace pserver.Templates
{
    public class MapManager
    {
        Dictionary<ushort, MapPlugin> MapList;

        public MapPlugin this[ushort key]
        {
            get
            {
                // If this key is in the dictionary, return its value.
                if (MapList.ContainsKey(key))
                {
                    // The key was found; return its value. 
                    return MapList[key];
                }
                else
                {
                    // The key was not found; return null. 
                    throw new Exception("No key in Dictionary");
                }
            }

            set
            {
                // If this key is in the dictionary, change its value. 
                if (MapList.ContainsKey(key))
                {
                    // The key was found; change its value.
                    MapList[key] = value;
                }
                else
                {
                    try
                    {
                        foreach (var t in (from c in Assembly.GetExecutingAssembly().GetTypes()
                                           where c.IsClass && c.IsSubclassOf(typeof(MapPlugin))
                                           select c))
                        {
                            var y = (Activator.CreateInstance(t) as MapPlugin);
                            if (y.MapID == key)
                            {
                                MapList.Add(y.MapID, y);
                                DLogger.SystemLog("Loaded Map " + y.MapID.ToString());
                            }
                        }
                    }
                    catch (Exception f) { DLogger.ErrorLog(f.StackTrace, f.Message); throw f; }
                }
            }
        }

        public MapManager()
        {
            MapList = new Dictionary<ushort, MapPlugin>();
        }

        public BindingList<Player> PlayerList
        {
            get
            {
                List<Player> tmp = new List<Player>();
                foreach (var h in MapList.ToList())
                    tmp.AddRange(h.Value.Players);
                return new BindingList<Player>(tmp);
            }
        }
        
        
        public void SendCurrentPlayers(Player to)
        {
            byte[] data = {100, 0, 0, 0, 0, 1, 1, 35, 39, 210, 2, 227, 3, 0, 0, 0, 28, 175,            
                125, 26, 28, 175, 125, 26, 0, 0, 0, 0, 0, 0, 0, 0, 8, 67,           
                117, 112, 105, 100, 49, 48, 48, 0, 255};
            SendPacket p = new SendPacket();
            p.Header(4);
            p.AddArray(data);
            to.Send(p);
            foreach (Player c in PlayerList)
            {
                p = new SendPacket();
                p.Header(4);
                p.AddDWord(c.CharacterID);
                p.AddByte((byte)c.Info.Body); //body style
                p.AddByte((byte)c.Info.Element); //element
                p.AddByte(c.Info.Level); //level
                p.AddWord(c.Info.currentMap.MapID); //map id
                p.AddWord(c.X_Axis); //x
                p.AddWord(c.Y_Axis); //y
                p.AddByte(0); p.AddByte(c.Info.Head); p.AddByte(0);
                p.AddWord(c.Info.HairColor);
                p.AddWord(c.Info.SkinColor);
                p.AddWord(c.Info.ClothingColor);
                p.AddWord(c.Info.EyeColor);
                p.AddByte(c.Eqs.WornCount);//clothesAmmt); // ammt of clothes
                p.AddArray(c.Eqs.Worn_Equips);
                p.AddDWord(0); p.AddByte(0); //??
                p.AddByte(BitConverter.GetBytes(c.Info.Reborn)[0]); //is rebirth
                p.AddByte((byte)c.Info.Job); //rb class
                p.AddString(c.CharacterName);//(BYTE*)c.name,c.nameLen); //name
                p.AddString(c.Nickname);//(BYTE*)c.nick,c.nickLen); //nickname
                p.AddByte(255); //??
                to.Send(p);
                to.onPlayerLogin(c);
                c.onPlayerLogin(to);
            }
        }
        
        public bool Player_login(ushort toMap, ref Player src)
        {
            if(MapList.ContainsKey(toMap))
                try { MapList[toMap].onLogin(ref src); return true; }
            catch (Exception d) { DLogger.ErrorLog(d.StackTrace, d.Message); }
            else
            {
                try
                {
                    foreach (var t in (from c in Assembly.GetExecutingAssembly().GetTypes()
                                       where c.IsClass && c.IsSubclassOf(typeof(MapPlugin))
                                       select c))
                    {
                        var y = (Activator.CreateInstance(t) as MapPlugin);
                        if (y.MapID == toMap)
                        {
                            y.onLogin(ref src); 
                            MapList.Add(y.MapID, y);
                            DLogger.SystemLog("Loaded Map " + y.MapID.ToString());
                            return true;
                        }
                    }
                }
                catch (Exception f) { DLogger.ErrorLog(f.StackTrace, f.Message); }
            }
            return false;
        }

        public bool Player_Teleport(WarpPortal toMap, ref Player src)
        {
            if (MapList.ContainsKey(toMap.DstMap))
                try { MapList[toMap.DstMap].onWarp_In(ref src,toMap); }
                catch (Exception d) { DLogger.ErrorLog(d.StackTrace, d.Message); }
            else
            {
                try
                {
                    foreach (var t in (from c in Assembly.GetExecutingAssembly().GetTypes()
                                       where c.IsClass && c.IsSubclassOf(typeof(MapPlugin))
                                       select c))
                    {
                        var y = (Activator.CreateInstance(t) as MapPlugin);
                        if (y.MapID == toMap.DstMap)
                        {
                            y.onWarp_In(ref src,toMap);
                            MapList.Add(y.MapID, y);
                            DLogger.SystemLog("Loaded Map " + y.MapID.ToString());
                            return true;
                        }
                    }
                }
                catch (Exception f) { DLogger.ErrorLog(f.StackTrace, f.Message); }
            }
            return false;
        }

        public void Broadcast(SendPacket pkt)
        {
            PlayerList.ToList().ForEach(new Action<Player>(c => c.Send(pkt)));
        }
        public void BroadcastEx(SendPacket pkt, uint ID)
        {
            PlayerList.Where(c => c.CharacterID != ID).ToList().ForEach(new Action<Player>(c => c.Send(pkt)));
        }

        public void UpdateALL()
        {
            MapList.ToList().ForEach(new Action<KeyValuePair<ushort, MapPlugin>>(c => c.Value.UpdateMap()));
        }
    }
}
