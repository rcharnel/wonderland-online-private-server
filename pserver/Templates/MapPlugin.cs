﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using pserver.Network;

namespace pserver
{
    public class MapPlugin
    {
        protected bool isRemoved;
        //BattleManager gBattleManager { get; }

        public Dictionary<byte, WarpPortal> Portallist { get { return new Dictionary<byte, WarpPortal>(); } }

        public List<Player> Players = new List<Player>();
        public virtual ushort MapID { get { return 0; } }
        public virtual string Name { get { return "None"; } }

        public virtual void onLogin(ref Player player)
        {
            player.Disconnected += player_Disconnected;
        }
        public virtual void onTerminated()
        {
            throw new NotImplementedException();
        }
        public virtual void onLoad()
        {
            throw new NotImplementedException();
        }

        public void Broadcast(SendPacket y)
        {
            Players.ToList().ForEach(new Action<Player>(c => c.Send(y)));
        }
        public void BroadcastEx(SendPacket y, uint ID)
        {
            Players.Where(c => c.CharacterID != ID).ToList().ForEach(new Action<Player>(c => c.Send(y)));
        }

        public virtual void UpdateMap()
        {
            throw new NotImplementedException();
        }

        public virtual bool Teleport(ref Player sender, byte portalID,out WarpPortal mapid)
        {
            WarpPortal door = null;
            try
            {
                door = Portallist[portalID];
            }
            catch { }

            if( door != null)
            {
                mapid = door;
                return true;
            }
            else
            {
                mapid = null;
                return false;
            }
        }

        public virtual void onWarp_In(ref Player src,WarpPortal from)
        {
            for (int a = 0; a < Players.Count; a++)
            {
                if (Players[a] == src) return;
            }
            src.Info.currentMap = this;
            src.Disconnected += player_Disconnected;


            for (int a = 0; a < Players.Count; a++)
            {
                //send to them
                SendPacket p = new SendPacket();
                p.Header(5, 0);
                p.AddDWord(src.CharacterID);
                p.AddArray(src.Eqs.Worn_Equips);
                Players[a].Send(p);
                p = new SendPacket();
                p.Header(10, 3);
                p.AddDWord(src.CharacterID);
                p.AddByte(255);
                Players[a].Send(p);//maybe guild info???
                p = new SendPacket();
                p.Header(5, 8);
                p.AddDWord(src.CharacterID);
                p.AddByte(0);
                Players[a].Send(p);

                //send to me
                p = new SendPacket();
                p.Header(7);
                p.AddDWord(Players[a].CharacterID);
                p.AddWord(MapID);
                p.AddWord(Players[a].X_Axis);
                p.AddWord(Players[a].Y_Axis);
                src.Send(p);
                p = new SendPacket();
                p.Header(5, 0);
                p.AddDWord(Players[a].CharacterID);
                p.AddArray(Players[a].Eqs.Worn_Equips);
                src.Send(p);
            }


        }
        public virtual void onWarp_Out(ref Player src, WarpPortal To)
        {
            src.Disconnected -= player_Disconnected;
            Players.Remove(src);
            SendAc12(src, To);
            
        }
        
        public  void SendMapInfo(Player t)
            {
                SendPacket p = new SendPacket();
                p = new SendPacket();
                p.Header(23, 138);
                t.Send(p);
                /* p = new SendPacket();
                 p.Header(6, 2);
                 p.AddDWord(1);
                 p.SetSize();
                 g.SendPacket(t, p);*/
                //SendNpcs(t);
                //SendItems(t);

                for (int a = 0; a < Players.Count; a++)
                {
                    p = new SendPacket();
                    p.Header(23, 122);
                    p.AddDWord(Players[a].CharacterID);
                    t.Send(p);
                    if (Players[a] != t)
                    {
                        p = new SendPacket();
                        p.Header(10, 3);
                        p.AddDWord(Players[a].CharacterID);
                        p.AddByte(255);
                        t.Send(p);


                        //if (characters_in_map[a].riceBall.id > 0)
                        //{
                        //    if (characters_in_map[a].riceBall.active) g.ac5.Send_5(characters_in_map[a].riceBall.id, characters_in_map[a], t);
                        //}
                        //if (t.riceBall.id > 0)
                        //{
                        //    if (t.riceBall.active) g.ac5.Send_5(t.riceBall.id, t, characters_in_map[a]);
                        //}

                        //if (t.MyTeam.PartyLeader && t.MyTeam.hasParty && plist[a] != t)
                        //{
                        //    SendPacket fg = t.MyTeam._13_6;
                        //    plist[a].Send(fg);
                        //}
                        //if (plist[a].MyTeam.PartyLeader && plist[a].MyTeam.hasParty)
                        //{
                        //    SendPacket fg = plist[a].MyTeam._13_6;
                        //    t.Send(fg);
                        //}
                        //if (WloPlayer.WloPlayerID != t.WloPlayerID)
                        //g.ac23.Send_74(WloPlayer.WloPlayerID, 0, c); //TODO find out what this does
                        //AC 15,4 //possibly pet info for players on map with pets

                        //if (plist[a].CharacterState == PlayerState.inBattle)
                        //{
                        //    SendPacket qp = new SendPacket(t);
                        //    qp.Header(11, 4);
                        //    qp.AddByte(2);
                        //    qp.AddDWord(plist[a].CharacterID);
                        //    qp.AddWord(0);
                        //    qp.AddByte(0);
                        //    qp.Send();
                        //}
                        //Send_32_2(t);
                        //23_76                    
                    }
                    p = new SendPacket();
                    p.Header(23, 76);
                    p.AddDWord(Players[a].CharacterID);
                    t.Send(p);
                }
                //39_9
                //SendPacket gh = new SendPacket(g);
                //gh.AddArray(new byte[] { 244, 68, 5, 0, 22, 6, 1, 0, 1, 244, 68, 5, 0, 22, 6, 21, 0, 1, 244, 68, 5, 0, 22, 6, 22, 0, 1, 244, 68, 5, 0, 22, 6, 23, 0, 1, 244, 68, 5, 0, 22, 6, 24, 0, 1, });
                // cServer.Send(gh, t);
                /*tmp = new SendPacket(g);
                tmp.Header(6, 2);
                tmp.AddByte(1);
                tmp.SetSize();
                tmp.WloPlayer = t;
                tmp.Send();
                for (int a = 0; a < 1; a++)
                {
                    gh = new SendPacket(g);
                    gh.AddArray(new byte[] { 244, 68, 2, 0, 20, 11, 244, 68, 2, 0, 20, 10 });
                    t.DatatoSend.Enqueue(gh);
                }
                for (int a = 0; a < 1; a++)
                {
                    gh = new SendPacket(g);
                    gh.AddArray(new byte[] { 244, 68, 2, 0, 20, 10 });
                    t.DatatoSend.Enqueue(gh);
                }*/
                p = new SendPacket();
                p.Header(23, 102);
                t.Send(p);
                p = new SendPacket();
                p.Header(20, 8);
                t.Send(p);
                //t.CharacterState = PlayerState.inMap;
            }


        #region Quick Data Send
        void SendAc12( Player target,WarpPortal To)
        {
            SendPacket sp = new SendPacket();
            sp.Header(12);
            sp.AddDWord(target.CharacterID);
            sp.AddWord(To.DstMap);
            sp.AddWord(To.DstX_Axis);
            sp.AddWord(To.DstY_Axis);
            sp.AddWord(To.ID);
            sp.AddByte(0);
            Players.ToList().ForEach(new Action<Player>(c => c.Send(sp)));
        }
        #endregion

        void player_Disconnected(object sender, EventArgs e)
        {
            try
            {
                Players.Remove(Players.Single(c => c.CharacterID == (sender as Player).CharacterID));
            }
            catch { }
        }
    }
}
