﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using pserver.Network;

namespace pserver
{
   public class Character :EquipementManager
    {
       public InventoryManager Inventory;
       public event onPacketSend DataOut;

        #region Body Info
        public UInt16 HairColor;
        public UInt16 SkinColor;
        public UInt16 ClothingColor;
        public UInt16 EyeColor;
        #endregion

        #region Character Info
        public UInt32 charID = 0;
        public string Name;
        public string Nickname;
        #endregion

        #region Map Related
        public UInt16 X;
        public UInt16 Y;
        public MapPlugin currentMap;
        //public WarpInfo prevMap;
        //public WarpInfo lastMap;
        //public WarpInfo recordMap;
        //public WarpInfo gpsMap;
        public ushort loginMap; // Used to designate the Map to Login too
        #endregion

        #region Guild
        public int Guild;
        #endregion
       
        public Character()
        {
            Clear();
            onDataOut += Character_onDataOut;
        }

        void Character_onDataOut(SendPacket pkt)
        {
            if (DataOut != null) DataOut(pkt);
        }
        public Character(object[] src)
        {
            Clear();
            onDataOut += Character_onDataOut;
            SetFromDB(src);
        }

        public void Clear()
        {
            Inventory = new InventoryManager();
            Nickname = "";
            currentMap = new MapPlugin();
            TotalExp = 6;
        }

       public void SetFromDB(object[] r)
        {
            try
            {
                System.Data.DataRow data = null;
                try { data = ((System.Data.DataRow[])r[0])[0]; }
                catch { }
                if (data != null)
                {
                    uint.TryParse(data["charID"].ToString(), out charID);
                    byte.TryParse(data["head"].ToString(), out Head);

                    byte b = 0;
                    byte.TryParse(data["body"].ToString(), out b);
                    Body = (BodyStyle)b;
                    Name = data["name"].ToString();
                    Nickname = data["nickname"].ToString();
                    ushort.TryParse(data["location_map"].ToString(), out loginMap);
                    ushort.TryParse(data["location_x"].ToString(), out X);
                    ushort.TryParse(data["location_y"].ToString(), out Y);
                    ushort.TryParse(data["haircolor"].ToString(), out HairColor);
                    ushort.TryParse(data["skincolor"].ToString(), out SkinColor);
                    ushort.TryParse(data["clothingcolor"].ToString(), out ClothingColor);
                    ushort.TryParse(data["eyecolor"].ToString(), out EyeColor);
                    uint.TryParse(data["gold"].ToString(), out Gold);

                    b = 0;
                    byte.TryParse(data["element"].ToString(), out b);
                    Element = (ElementType)b;

                    b = 0;
                    byte.TryParse(data["job"].ToString(), out b);
                    Job = (RebornJob)b;
                }

                List<uint[]> stats = null;

                try { stats = (List<uint[]>)r[1]; }
                catch { }

                if (stats != null)
                    LoadStats(stats);

                Dictionary<byte, string[]> eq = null;
                try { eq = (Dictionary<byte, string[]>)r[3]; }
                catch { }

                if(eq != null && eq.Count > 0)
                    foreach(var t in eq)
                    {
                        InvItemCell tmp = new InvItemCell();
                        tmp.CopyFrom(cGlobal.WloItemManager.GetItem(ushort.Parse(t.Value[0])));
                        tmp.Ammt = 1;
                        tmp.Damage = byte.Parse(t.Value[4]);
                        SetEQ(byte.Parse(t.Value[6]), tmp);
                    }
            }
            catch (Exception f) { DLogger.ErrorLog(f.StackTrace, f.Message); }
        }
       
       #region Equips
        public void SetBy9_1(RecvPacket p)
        {
            Str = p.GetByte(15);
            Agi = p.GetByte(16);
            Wis = p.GetByte(17);
            Int = p.GetByte(18);
            Con = p.GetByte(19);
        }

        //public void CalcAIFullStats() //this function sets the stas according to pts

        //{

        //    get
        //    {
        //        Int32 nRet = 0;
        //        for (int i = 0; i < 6; ++i)
        //            nRet += clothes[i].HP;
        //        return nRet;
        //    }

        //    sFullAtk = (ushort)(sAtk + (Level / 5) * 1.9);
        //    sFullDef = (ushort)(sDef + (Level / 5) * 1.9);
        //    sFullSpd = (ushort)(sSpd + (Level / 5) * 1.9);
        //    sFullMdef = (ushort)(sMdef + (Level / 5) * 1.9);
        //    sFullMatk = (ushort)(sMatk + (Level / 5) * 1.9);
        //    hpPlus = 0;
        //    spPlus = 0;

        //}

        
        #endregion
       
    }
}

