﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using pserver.Network;

namespace pserver.Ac
{
    public class AC23 : AC
    {
        public override int ID
        {
            get
            {
                return 23;
            }
        }
        public override void Switchboad(ref Player p, RecvPacket r)
        {
            switch (r.b)
            {
                case 10: Recv_10(ref p,r); break; //Item Moved in Inventory
                case 11: Recv_11(ref p, r); break;
                case 12: Recv_12(ref p, r); break;
            }
        }


        public void Recv_10(ref Player p, RecvPacket r) //item moved in inventory
        {
            byte src = r.Data[2];
            byte ammt = r.Data[3];
            byte dst = r.Data[4];

            if (((src > 0) && (src < 51)) && ((dst > 0) && (dst < 51)) && ((ammt > 0) && (ammt < 51)))
                p.Inv.MoveItem(src, dst, ammt);
        }
        public void Recv_11(ref Player p, RecvPacket r) //item selected to wear in inv
        {
            byte loc = r.Data[2];
            if ((loc > 0) && (loc < 51))
            {
                //TODO do any checks here to make sure we can wear item or in Equipment class
                p.WearEQ(loc);
            }
        }
        public void Recv_12(ref Player p, RecvPacket r) //item selected to remove
        {
            byte loc = r.Data[2];
            byte dst = r.Data[3];
            if ((loc > 0) && (loc < 7) && (dst >0) && (dst <51))
            {
                p.unWearEQ(loc, dst);
            }
        }
    }
}
