﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using pserver.Network;

namespace pserver.Ac
{
    public class AC02 : AC
    {
        public override int ID
        {
            get
            {
                return 2;
            }
        }
        public override void Switchboad(ref Player p, RecvPacket r)
        {
            switch (r.b)
            {
                case 2: Recv_2(ref p, r); break;
                default: base.Switchboad(ref p, r); break;
            }
        }

        public void Recv_2(ref Player p, RecvPacket r) //local chat
        {
            string str = r.GetStringRaw(2, r.Size - 2);

            string[] words = str.Split(' ');

            if (words.Length >= 1)
            {

                switch (words[0])
                {
                    #region Gm Cmd Warp
                    /* case ":warp":
                         {
                             cWarp w = new cWarp();
                             UInt16 value = 0;
                             try { value = UInt16.Parse(words[1]); }
                             catch { }
                             w.x = 500; w.y = 500; w.mapTo = value; w.id = 0;
                             g.gDataManager.MapManager.GetMapByID(g.packet.cCharacter.mapLoc).WarpRequest(WarpType.SpecialWarp,g.packet.cCharacter,0,w);
                             //g.ac20.DoWarp(w);
                                    
                         } break;*/
                    #endregion
                    #region Gm Cmd Item
                    case ":item":
                        {
                            switch (words[1])
                            {
                                case "add":
                                    {
                                        byte ammt = 1;
                                        int ct = words.Length;
                                        UInt16 itemid = 0;
                                        if (ct > 2)
                                        {

                                            try { itemid = UInt16.Parse(words[2]); }
                                            catch { }
                                            if (ct > 2)
                                            {
                                                try { ammt = byte.Parse(words[3]); }
                                                catch { }
                                            }
                                            InvItemCell i = new InvItemCell();
                                            i.CopyFrom(cGlobal.WloItemManager.GetItem(itemid));
                                            i.Ammt = ammt;

                                            p.Inv.AddItem(i);
                                        }

                                    } break;
                                case "rem":
                                    {
                                        byte at = 0;
                                        byte ammt = 1;
                                        int ct = words.Length;
                                        if (ct > 2)
                                        {
                                            try { byte.TryParse(words[2], out at); }
                                            catch { }
                                            try { ammt = byte.Parse(words[3]); }
                                            catch { }

                                            p.Inv.RemoveItem(at, ammt);
                                        }

                                    }break;
                                case "clear": p.Inv.RemoveAll(); break;
                            }
                            
                        } break;
                    #endregion
                    #region Gm Cmd RiceBall
                    //         case ":npc":
                    //             {
                    //                 UInt16 npcid = 0; int ct = words.Length;
                    //                 if (ct > 1)
                    //                 {
                    //                     try { npcid = UInt16.Parse(words[1]); }
                    //                     catch { }
                    //                     c.riceBall.GMRiceBall(npcid, c);
                    //                     g.ac5.Send_5(npcid, c, c);

                    //                      {
                    //cInvItem i = new cInvItem(g);
                    //i = c.GetInventoryItem(cell); i.ammt = 1;
                    //c.riceBall.Start(cell, i, c);
                    //}
                    //                 }

                    //             } break;
                    #endregion
                    #region GM Cmd Battle
                    //case ":fight":
                    //    {
                    //        //cBattle b = new cBattle(g);
                    //        //b.Test(c);
                    //    } break;
                    //case ":next":
                    //    {
                    //        //cBattle b = c.battle;
                    //        //if (b.active) b.StartRound();
                    //    } break;
                    //case ":end":
                    //    {
                    //        //cBattle b = new cBattle(g);
                    //        //b.TestExit(c);
                    //    } break;*/
                    #endregion
                    default:
                        {
                            SendPacket tmp = new SendPacket();
                            tmp.Header(2, 2);
                            tmp.AddDWord(p.CharacterID);
                            tmp.AddString(str, false);
                            cGlobal.World.BroadcastEx(tmp, p.CharacterID);
                        } break;
                }

            }
            else
            {
                SendPacket tmp = new SendPacket();
                tmp.Header(2, 2);
                tmp.AddDWord(p.CharacterID);
                tmp.AddString(str, false);
                cGlobal.World.BroadcastEx(tmp, p.CharacterID);
            }
        }
    }
}
