﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using pserver.Network;

namespace pserver.Ac
{
    class AC06:AC
    {
        public AC06()
        {
        }

        public override int ID
        {
            get
            {
                return 6;
            }
        }
        public override void Switchboad(ref Player p, RecvPacket r)
        {
            switch (r.b)
            {
                case 1:
                    {
                        Recv_1(ref p, r);
                    } break;
                default: base.Switchboad(ref p, r); break;
            }
            
        }
        public void Recv_1(ref Player p,RecvPacket g)
        {
            byte direction = g.Data[2];
            p.Info.X = g.GetWord(3);
            p.Info.Y = g.GetWord(5);
            //WORD unknown = p->GetWord(7);
            //p.Info.e = 0;
            SendPacket tmp = new SendPacket();
            tmp.Header(6, 1);
            tmp.AddDWord(p.CharacterID);
            tmp.AddByte(direction);
            tmp.AddWord(p.X_Axis);
            tmp.AddWord(p.Y_Axis);
            p.Info.currentMap.BroadcastEx(tmp, p.CharacterID);
        }
    }
}
