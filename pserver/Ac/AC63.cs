﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using pserver.Network;

namespace pserver.Ac
{
    public class AC63 : AC
    {
        public override int ID
        {
            get
            {
                return 63;
            }
        }
        public override void Switchboad(ref Player p, RecvPacket r)
        {
            switch (r.b)
            {
                case 0:
                case 3: Recv3(ref p, r); break;
                case 2: Recv2(ref p, r); break;
                case 4: Recv4(ref p, r); break;
                default: base.Switchboad(ref p, r); break;
            }
        }

        void Recv3(ref Player p, RecvPacket r)
        {
            p.DbuserID = 0;
            p.userName = "";
            p.Slot = 0;
            p.UserLoc = UserDBLoc.NONE;
            cGlobal.CharacterDB.unLockName(p.CharacterName);

        }

        void Recv2(ref Player p, RecvPacket e)
        {
            byte charNum = e.GetByte(2);


            if ((charNum < 1) || (charNum > 2) || !p.isConnected)//by userid
            {
                p.DbuserID = 0;
                p.userName = "";
                p.Slot = 0;
                 p.UserLoc = UserDBLoc.NONE;
                SendPacket tmp = new SendPacket();
                tmp.Header(0, 32);
                p.Send(tmp);
                return;
            }
            p.Slot = charNum;

            if ((p.GetSlotID(charNum) == 0 && cGlobal.CharacterDB.GetData(p.GetSlotID(charNum)) == null) && !cGlobal.DEVMODE) // char is not created
            {
                #region Create Character
                
                p.myCharlist[charNum] = new Character();

                switch (p.UserLoc)
                {
                    case UserDBLoc.WLO4EVER: cGlobal.WLO4EVERDB.Update_Player_ID(p.DbuserID, p.CharacterID, charNum); break;
                    case UserDBLoc.TEST: cGlobal.TestDB.Update_Player_ID(p.DbuserID, p.CharacterID, charNum); break;
                }
                //p..CharacterState = GameCharacter.PlayerState.Creating;
                SendPacket tmp = new SendPacket();
                tmp.Header(1, 3);
                tmp.AddBool((!string.IsNullOrEmpty(p.char_delete_code)));
                p.Send(tmp);
                #endregion
            }
            else
            {
                #region Login

                if (p.Info != null && p.CharacterID != 0)
                {
                    //var r = cGlobal.GameDB.Src.GetRow("inventory", "invID = " + p.CharacterID);
                    //if (r != null)
                    //{
                    //    string heading = "";
                    //    for (byte a = 1; a < 51; a++)
                    //    {
                    //        heading = "inv" + a;
                    //        string istr = (string)r[heading]; istr = istr.Trim();
                    //        if (istr.Length > 0)
                    //        {
                    //            string[] words = istr.Split(' ');
                    //            int ct = words.Length;
                    //            if (ct > 1) p.Inv[a].Ammt = byte.Parse(words[1]);
                    //            if (ct > 2) p.Inv[a].Damage = byte.Parse(words[2]);
                    //            if (ct > 3) p.Inv[a].ParentSlot = byte.Parse(words[3]);
                    //            p.Inv[a].CopyFrom(cGlobal.ItemManager.GetItem(ushort.Parse(words[0])));
                    //            //if (mainInv[n[0]][n[1]].itemtype.Veichle)
                    //            //    own.vechile.Add(mainInv[n[0]][n[1]].ID, a);
                    //        }

                    //    }
                    //    for (byte n = 1; n < 7; n++)
                    //    {
                    //        heading = "wear" + n;
                    //        string istr = (string)r[heading]; istr = istr.Trim();
                    //        if (istr.Length > 0)
                    //        {
                    //            string[] words = istr.Split(' ');
                    //            int ct = words.Length;
                    //            if (ct > 0) p.Eqs[n].CopyFrom(cGlobal.ItemManager.GetItem(UInt16.Parse(words[0])));
                    //            if (ct > 1) p.Eqs[n].Ammt = byte.Parse(words[1]);
                    //            if (ct > 2) p.Eqs[n].Damage = byte.Parse(words[2]);
                    //            if (ct > 3) p.Eqs[n].ParentSlot = byte.Parse(words[3]);
                    //        }

                    //    }
                    //}
                    //else
                    //    cGlobal.GameDB.CreateInventory(p);

                    SendPacket tmp = new SendPacket();
                    tmp.Header(63, 2);
                    tmp.AddDWord(p.UserID);
                    p.Send(tmp);
                    NormalLog(p);
                }
                else
                {
                    SendPacket tmp = new SendPacket();
                    tmp.Header(0, 8);
                    p.Send(tmp);
                }
                #endregion
            }
        }

        void Recv4(ref Player p, RecvPacket r)
        {
            int loginState = 0; //0-good login  1-bad un/pw  2-dup log 3-wrong version 4-need update

            //sending username and password
            int at = 2;

            string name = r.GetString(at); at += name.Length + 1;
            string password = r.GetString(at); at += password.Length + 1;
            name = DataBase.Security.CleanString(name).ToLower();
            password = DataBase.Security.CleanString(password);

            string[] userdata = new string[1];//data of user

            UInt16 version = r.GetWord(at); at += 2;
            byte lcLen = r.Data[at]; at++;
            byte key = r.Data[at]; at++;
            char[] lCode = new char[20];
            Array.Copy(r.Data.ToArray(), at, lCode, 0, lcLen);
            for (int n = 0; n < lcLen; n++)
                lCode[n] = (char)((byte)lCode[n] ^ (byte)key);

            if ((name.Length < 4) || (name.Length > 14))
            {
                loginState = 1;
            }
            else if ((password.Length < 4) || (password.Length > 14))
            {
                loginState = 1;
            }

            else if (version < 1096)
            { //bad aloign version
                loginState = 3;
            }
            else if ((lcLen < 2) || (lcLen > 15))
            { //bad login code length
                loginState = 4;
            }

            //at this point we have no use, and only an empty cCharacter object
            //check to see if we are still in the clear for loggin ing
            if (loginState == 0)
            {
                //should have correct version if at this point

                uint userid = 0;
                //check if user exist on wloforever
                if (!cGlobal.DEVMODE)
                {
                    #region Validate Account
                    if (cGlobal.WLO4EVERDB.isValidAccount(name))
                    {

                        if (cGlobal.WLO4EVERDB.isValidAccount(name, password, out userid))
                        {
                            if (userid == 0)
                                loginState = 5;
                            else
                            {
                                if (cGlobal.PlayerList.Count(c => c.DbuserID == userid) > 0)
                                {
                                    //TODO implement Disconnect
                                    loginState = 2;
                                }
                                else
                                {
                                    userdata = cGlobal.WLO4EVERDB.GetUserData(userid, password);
                                        p.DbuserID = userid;
                                        p.userName = userdata[0];
                                        p.UserLoc = UserDBLoc.WLO4EVER;
                                        p.char_delete_code = userdata[3];
                                }
                            }
                        }
                        else
                            loginState = 1;

                    }
                    else if (cGlobal.TestDB.isValidAccount(name))//if user is from testdb
                    {
                        if (cGlobal.TestDB.isValidAccount(name, password, out userid))
                        {
                            if (userid == 0)
                                loginState = 5;
                            else
                            {
                                if (cGlobal.PlayerList.Count(c => c.DbuserID == userid) > 0)
                                {
                                    //TODO implement Disconnect
                                    loginState = 2;
                                }
                                else
                                {
                                    userdata = cGlobal.TestDB.GetUserData(userid, password);
                                    p.DbuserID = userid;
                                    p.userName = userdata[0];
                                    p.UserLoc = UserDBLoc.TEST;
                                    p.char_delete_code = userdata[3];
                                }
                            }
                        }
                        else
                            loginState = 1;
                    }
                    else
                        loginState = 5;
                    #endregion
                }
                else
                {
                    userid = 1000;
                    p.DbuserID = userid;
                    loginState = 0;
                    userdata = new string[] { "DevTest", "1000", "0", "123456", "" };
                }


                
            }

            #region Result of Login State
            // here we do the results of loginstate
            switch (loginState)
            {
                case 0:
                    {
                        uint char1 = 0;
                        uint char2 = char1;

                        uint.TryParse(userdata[1], out char1);
                        uint.TryParse(userdata[2], out char2);

                        SendPacket tmp = new SendPacket();
                        tmp.Header(63, 2);
                        tmp.AddDWord(p.UserID);
                        p.Send(tmp);
                        if (!cGlobal.DEVMODE)
                            p.myCharlist[1] = new Character(cGlobal.CharacterDB.GetData(char1));
                        else
                        {
                            p.myCharlist[1] = new Character()
                            {                                
                                charID = 1000,
                                Head = 1,
                                Body = (BodyStyle)3,
                                Name = "TestChar",
                                TotalExp = 6,
                                CurHP = 180,
                                CurSP = 94,
                                loginMap = 10017,
                                X = 462,
                                Y = 1255,
                                Str = 5,
                                Con = 7,
                                Int = 4,
                                Gold = 10000,
                                Nickname = "",
                                HairColor = 44828,
                                SkinColor = 6781,
                                ClothingColor = 44828,
                                EyeColor = 6781,
                                Element = (ElementType)1,
                                Job = RebornJob.none
                            };
                        }
                        if (!cGlobal.DEVMODE)
                            p.myCharlist[2] = new Character(cGlobal.CharacterDB.GetData(char2));
                        else
                            p.myCharlist[2] = new Character();
                        byte[] data1 = null;
                        byte[] data2 = null;
                        if (p.myCharlist[1] != null && p.myCharlist[1].charID != 0) { data1 = Get_63_1Data(p.myCharlist[1], 1); }
                        if (p.myCharlist[2] != null && p.myCharlist[2].charID != 0) { data2 = Get_63_1Data(p.myCharlist[2], 2); }

                        tmp = new SendPacket();
                        tmp.Header(63, 1);
                        if (data1 != null) tmp.AddArray(data1);
                        if (data2 != null) tmp.AddArray(data2);
                        p.Send(tmp);
                        //user.CharacterState = GameCharacter.PlayerState.CharacterSelection;
                        tmp = new SendPacket();
                        tmp.Header(35, 11);
                        p.Send(tmp);

                    } break;
                case 1:
                    {
                        SendPacket tmp = new SendPacket();
                        tmp.Header(63, 2);
                        tmp.AddDWord(0);
                        p.Send(tmp);
                        tmp = new SendPacket();
                        tmp.Header(1, 6);
                        p.Send(tmp);
                    } break;
                case 2:
                    {
                        //if (status != null) status("Server", "Already logged in. ( " + p.UserName + " )");
                        SendPacket tmp = new SendPacket();
                        tmp.Header(63, 2);
                        tmp.AddDWord(p.CharacterID);
                        p.Send(tmp);
                        SendPacket sp = new SendPacket();// PSENDPACKET PackSend = new SENDPACKET;
                        //PackSend->Clear();
                        sp.Header(0, 19);//PackSend->Header(63,2);
                        p.Send(sp);
                    } break;
                case 3:
                    {
                        SendPacket sp = new SendPacket();// PSENDPACKET PackSend = new SENDPACKET;
                        //PackSend->Clear();
                        sp.Header(0, 17);//PackSend->Header(63,2);
                        p.Send(sp);
                    } break;
                case 4:
                    {
                        SendPacket sp = new SendPacket();// PSENDPACKET PackSend = new SENDPACKET;
                        //PackSend->Clear();
                        sp.Header(0, 65);//PackSend->Header(63,2);
                        p.Send(sp);
                    } break;
                case 5:
                    {
                        SendPacket sp = new SendPacket();// PSENDPACKET PackSend = new SENDPACKET;
                        //PackSend->Clear();
                        sp.Header(1, 7);//PackSend->Header(63,2);
                        p.Send(sp);
                    } break;
            }
            #endregion
        }

        #region wlo methods
        public byte[] Get_63_1Data(Character player, byte slot)
        {
            SendPacket temp = new SendPacket();
            temp.AddByte(slot);// data[at] = slot; at++;//PackSend->AddByte(1);
            temp.AddString(player.Name);// data[at] = nameLen; at++;
            temp.AddByte((byte)player.Level);// data[at] = level; at++;//	PackSend->AddByte(tmp1.level);					// Level 
            temp.AddByte((byte)player.Element);// data[at] = element; at++;//	PackSend->AddByte(3);  					// element
            temp.AddDWord((uint)player.FullHP);// putDWord(maxHP, data + at); at += 4;//	PackSend->AddDWord(tmp1.maxHP); 			// max hp
            temp.AddDWord((uint)player.CurHP);// putDWord(curHP, data + at); at += 4;//	PackSend->AddDWord(tmp1.curHP); 			// cur hp
            temp.AddDWord((uint)player.FullSP);// putDWord(maxSP, data + at); at += 4;//	PackSend->AddDWord(tmp1.maxSP); 			// max sp
            temp.AddDWord((uint)player.CurSP);// putDWord(curSP, data + at); at += 4;//	PackSend->AddDWord(tmp1.curSP); 			// cur sp
            temp.AddDWord(player.TotalExp);// putDWord(experience, data + at); at += 4;//	PackSend->AddDWord(tmp1.exp);			// exp
            temp.AddDWord(player.Gold);// putDWord(gold, data + at); at += 4;//	PackSend->AddDWord(tmp1.gold); 			// gold
            temp.AddByte((byte)player.Body);// data[at] = body; at++;//	PackSend->AddByte(tmp1.body); 					// body style
            temp.AddByte(0);
            temp.AddByte(player.Head);
            temp.AddByte(0);// data[at] = 0; data[at + 1] = head; data[at + 2] = 0; at += 3;//	PackSend->AddArray(tmp1.hair,3);// hair style
            temp.AddWord(player.HairColor);// putDWord(color1, data + at); at += 4;//	PackSend->AddDWord(tmp1.colors1);
            temp.AddWord(player.SkinColor);
            temp.AddWord(player.ClothingColor);
            temp.AddWord(player.EyeColor);
            temp.AddByte(BitConverter.GetBytes(player.Reborn)[0]);
            temp.AddByte((byte)player.Job);// data[at] = rebirth; data[at + 1] = job; at += 2;//PackSend->AddByte(tmp1.rebirth);PackSend->AddByte(tmp1.rebirthJob); 				// rebirth flag, job skill
            for (byte a = 1; a < 7; a++)
                temp.AddWord(player[a].ItemID);

            if (temp.PacketSize < 1) return null;
            return temp.Data;
        }
        public void NormalLog(Player c)
        {
            //c.CharacterState = GameCharacter.PlayerState.Logging_In;
            //a connection request was recieved
            c.DataOut = SendType.Multi;
            SendPacket p = new SendPacket();
            p.Header(20, 8);
            c.Send(p);

            Send_24_5(c, 183);
            Send_24_5(c, 53);
            Send_24_5(c, 52);
            Send_24_5(c, 54);
            Send_70_1(ref c, 23, "Something", 194);
            p = new SendPacket();
            p.Header(20, 33);
            p.AddByte(0);
            c.Send(p);
            //--------------------------Player PreInfo
            c.Info.Send8_1();
            p = new SendPacket();
            p.Header(14, 13);
            p.AddByte(3);
            c.Send(p);
            //------------------------Im Mall List
            //g.ac75.Send_1(g.gImMall_Manager.Get_75IM);
            p = new SendPacket();
            p.Header(75, 8);
            p.AddWord(0);
            c.Send(p);
            //------------------------
            c.Send_3_Me();
            cGlobal.World.SendCurrentPlayers(c);
            //send sidebar
            c.Send_5_3();
            c.Send_Inventory();
            p = new SendPacket();
            p.Header(23, 11);
            p.AddArray(c.Eqs._23_11Data);
            c.Send(p);
            SendPacket g = new SendPacket();
            //    g.Header(24, 6);
            //    g.AddArray(new byte[] { 001, 008, 047, 001, 002, 244, 050, 001, 003, 012, 043, 001 });
            //    g.SetSize();
            //    Send(g);
            //    g = new SPacket();
            //    g.Header(53, 10);
            //    g.AddArray(new byte[] { 032, 164, 036, 002, 037, 240, 038, 041, 058, 048, 083, 015 });
            //    g.SetSize();
            //    Send(g);
            //    g = new SPacket();
            //    g.Header(26, 7);
            //    g.AddArray(new byte[] { 001, 002, 002, 128, 003, 002, 004, 128 ,008,
            //                066, 009, 096, 010, 008, 011, 010 ,013, 001 });
            //    g.SetSize();
            //    Send(g);
            p = new SendPacket();
            p.Header(26, 4);
            p.AddDWord(c.Gold);
            c.Send(p);
            p = new SendPacket();
            p.Header(33, 2);
            p.AddArray(c.Settings.Data);
            c.Send(p);
            c.UpdateClient_FriendList();

            //pets
            //-----------------------------------   
            //---------Warp Info---------------------------------------------------
            //put me in my maps list
            if (!cGlobal.World.Player_login(c.Info.loginMap, ref c))
            {
                p = new SendPacket();
                p.Header(1, 7);
                c.Send(p);
                return;
            }


            p = new SendPacket();
            p.Header(5, 15);
            p.AddByte(0);
            c.Send(p);
            p = new SendPacket();
            p.Header(62, 53);
            p.AddWord(2);
            c.Send(p);
            p = new SendPacket();
            p.Header(5, 21);
            p.AddByte(c.Slot);//hmmmmm
            c.Send(p);
            p = new SendPacket();
            p.Header(5, 11);
            p.AddDWord(15085);//hmmmmm
            p.AddWord(5000);
            c.Send(p);
            //g.ac5.Send_11(15085, 0);//244, 68, 8, 0, 5, 11, 237, 58, 0, 0, 0, 0,         

            //---------------------------------

            //g.ac62.Send_4(g.packet.cCharacter.cCharacterID); //tent items

            //--------------------------------------
            p = new SendPacket();
            p.Header(5, 14);
            p.AddByte(2);
            c.Send(p);
            p = new SendPacket();
            p.Header(5, 16);
            p.AddByte(2);
            c.Send(p);
            var time = DateTime.Now.ToOADate();
            Send_23_140(ref c, 3, time);
            Send_25_44(ref c, 1, time);
            //g.ac23.Send_106(1, 1);
            Send_SingleByte_AC(ref c, 23, 160, (byte)3);
            Send_SingleByte_AC(ref c, 75, 7, (byte)1);
            Send_57("", ref c);
            p = new SendPacket();
            p.Header(69, 1);
            p.AddByte(71);
            c.Send(p);
            p = new SendPacket();
            p.Header(20, 60);
            p.AddByte(1);
            c.Send(p);
            p = new SendPacket();
            p.Header(66, 1);
            p.AddArray(new byte[] { 001, 012, 043, 000, 000, 000, 000, 000, 000, 000, 000 });
            c.Send(p);
            for (byte a = 1; a < 11; a++)
                Send_5_13(ref c, a, 0);
            for (byte a = 1; a < 11; a++)
                Send_5_24(ref c, a, 0);
            p = new SendPacket();
            p.Header(23, 162);
            p.AddByte(2);
            p.AddWord(0);
            c.Send(p);
            p = new SendPacket();
            p.Header(26, 10);
            p.AddDWord(0);
            c.Send(p);
            Send_SingleByte_AC(ref c, 23, 204, (ushort)1);
            p = new SendPacket();
            p.Header(23, 208);
            p.AddByte(2);
            p.AddByte(3);
            p.AddDWord(0);
            c.Send(p);
            p = new SendPacket();
            p.Header(23, 208);
            p.AddByte(2);
            p.AddByte(4);
            p.AddDWord(0);
            c.Send(p);
            p = new SendPacket();
            p.Header(1, 11);
            c.Send(p);
            p = new SendPacket();
            p.Header(15, 19);
            p.AddArray(new byte[] { 4, 6, 9, 94 });
            c.Send(p);

            //--------------------------------
            /*p = new SendPacket();
            p.Header(20, 33);
            p.AddByte(0);
            p.SetSize();
            c.Send(p);*/
            p = new SendPacket();
             p.Header(54);
             p.AddArray(
             new byte[] {
                 89,2,2,90,2,1,91,2,1,189,2,2,190,2,1,191,2,1});
             c.Send(p);
            c.UpdateClientIM();// IM Points 
            p = new SendPacket();
            p.Header(90, 1);
            p.AddArray(new byte[] { 000, 2, 2, 3 });
            c.Send(p);
            p = new SendPacket();
            p.Header(5, 4);
            c.Send(p);
            c.DataOut = SendType.Normal;
            //c.CharacterState = GameCharacter.PlayerState.inMap;
            //---------------------------------
        }
       

        public void Send_SingleByte_AC(ref Player player, byte ac, byte subac, object byteval)
        {
            SendPacket p = new SendPacket();
            p.Header(ac, subac);
            if (byteval is byte)
            {
                p.AddByte((byte)byteval);
            }
            else if (byteval is ushort)
            {
                p.AddWord((ushort)byteval);
            }
            else if (byteval is UInt32)
            {
                p.AddDWord((UInt32)byteval);
            }
            player.Send(p);

        }
        public void Send_SingleByte_AC(ref Player player, byte ac, object byteval)
        {
            SendPacket p = new SendPacket();
            p.Header(ac);
            if (byteval is byte)
            {
                p.AddByte((byte)byteval);
            }
            else if (byteval is ushort)
            {
                p.AddWord((ushort)byteval);
            }
            player.Send(p);

        }
        public void Send_57(string text, ref Player o) //sends sytem prompt message
        {
            SendPacket p = new SendPacket();
            p.Header(23, 57);
            p.AddByte(152);
            p.AddString(text,false);
            o.Send(p);
        }
        public void Send_24_5(Player player, byte value)
        {
            SendPacket p = new SendPacket();
            p.Header(24, 5);
            p.AddByte(value);
            p.AddWord(0);
            player.Send(p);

        }
        public void Send_70_1(ref Player player, byte value, string name, UInt16 id)
        {
            SendPacket p = new SendPacket();
            p.Header(70, 1);
            p.AddByte(value);
            p.AddString(name);
            p.AddWord(id);
            p.AddByte(0);
            player.Send(p);

        }
        public void Send_23_140(ref Player player, byte val, Double t)//time related
        {
            SendPacket p = new SendPacket();
            p.Header(23, 140);
            p.AddByte(val);
            p.AddDouble(t);
            player.Send(p);
        }
        public void Send_25_44(ref Player player, byte val, double v)//time related
        {
            SendPacket p = new SendPacket();
            p.Header(25, 44);
            p.AddByte(val);
            p.AddDouble(v);
            player.Send(p);
        }
        public void Send_5_13(ref Player player, byte value, UInt16 wVal)
        {
            SendPacket p = new SendPacket();
            p.Header(5, 13);
            p.AddByte(value);
            p.AddWord(wVal);
            player.Send(p);
        }
        public void Send_5_24(ref Player player, byte Val, UInt16 wVal)
        {
            SendPacket p = new SendPacket();
            p.Header(5, 24);
            p.AddByte(Val);
            p.AddWord(wVal);
            player.Send(p);
        }
        #endregion

    }
}
