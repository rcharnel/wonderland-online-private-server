﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using pserver.Network;

namespace pserver.Ac
{
    public class AC35 : AC
    {
        public override int ID
        {
            get
            {
                return 35;
            }
        }
        public override void Switchboad(ref Player p, RecvPacket r)
        {
            switch (r.b)
            {
                case 2: Recv2(ref p, r); break;
            }
        }

        void Recv2(ref Player p, RecvPacket e)
        {
            if (!p.isConnected) return;

            string pw = e.GetString(14);
            byte slot = e.GetByte(2);

            if (p.char_delete_code == pw)
            {
                if (cGlobal.CharacterDB.DeleteCharacter(p.GetSlotID(slot)))
                {
                    p.myCharlist[slot] = new Character();

                    if (p.GetSlotID(1) == 0 && p.GetSlotID(2) == 0)
                        p.char_delete_code = "";

                    switch (p.UserLoc)
                        {
                            case UserDBLoc.WLO4EVER:
                                {
                                    cGlobal.WLO4EVERDB.Update_Player_ID(p.DbuserID, 0, slot);
                                    cGlobal.WLO4EVERDB.UpdateUser(p.DbuserID, p.char_delete_code);
                                } break;
                            case UserDBLoc.TEST:
                                {
                                    cGlobal.TestDB.Update_Player_ID(p.DbuserID, 0, slot);
                                    cGlobal.TestDB.UpdateUser(p.DbuserID, p.char_delete_code);
                                } break;
                        }

                    Send_24_5(p, 53);
                    Send_24_5(p, 52);
                    Send_24_5(p, 54);
                    Send_24_5(p, 183);
                    SendPacket tmp = new SendPacket();
                    tmp.Header(20, 8);
                    p.Send(tmp);
                    tmp = new SendPacket();
                    tmp.Header(35, 2);
                    tmp.AddByte(1);
                    tmp.AddByte(slot);
                    p.Send(tmp);
                }
                else
                {
                    SendPacket tmp = new SendPacket();
                    tmp.Header(35, 2);
                    tmp.AddByte(3);
                    tmp.AddByte(slot);
                    p.Send(tmp);
                }
            }
            else
            {
                SendPacket tmp = new SendPacket();
                tmp.Header(35, 2);
                tmp.AddByte(3);
                tmp.AddByte(slot);
                p.Send(tmp);
            }

        }

        public void Send_24_5(Player player, byte value)
        {
            SendPacket p = new SendPacket();
            p.Header(24, 5);
            p.AddByte(value);
            p.AddWord(0);
            player.Send(p);

        }
    }
}
