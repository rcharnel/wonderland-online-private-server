﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using pserver.Network;



namespace pserver.Ac
{
    public class AC09 : AC
    {
        public override int ID
        {
            get
            {
                return 9;
            }
        }

        public override void Switchboad(ref Player p, RecvPacket r)
        {
            switch (r.b)
            {
                case 1: Recv1(ref p, r); break;
                case 2: Recv2(ref p, r); break;
            }
        }

        void Recv1(ref Player tp, RecvPacket e)
        {
            try
            {
                if (string.IsNullOrEmpty(tp.char_delete_code))
                {
                    tp.char_delete_code = e.GetString(20);
                    if ((tp.char_delete_code.Length < 6) ||
                        (tp.char_delete_code.Length > 14))
                    {
                        //make sure no save of data by setting values to 0
                        tp.Slot = 0;
                        SendPacket p = new SendPacket();
                        p.Header(0, 30);
                        tp.Send(p);
                        return;
                    }
                }

                tp.Info.Element = (ElementType)e.GetByte(14);

                tp.Info.Body = (BodyStyle)e.GetByte(2);
                tp.Info.Head = e.GetByte(4);
                tp.Info.HairColor = e.GetWord(6);
                tp.Info.SkinColor = e.GetWord(8);
                tp.Info.ClothingColor = e.GetWord(10);
                tp.Info.EyeColor = e.GetWord(12);

                tp.Info.SetBy9_1(e);
                foreach (cItem u in GetNoobClothes(tp))
                {
                    InvItemCell i = new InvItemCell();
                    i.CopyFrom(u);
                    tp.Info.SetEQ((byte)u.Data.EquipPos, i);
                }
                tp.FillHP(); tp.FillSP();
                tp.Settings.Set(3, 31);
                tp.Settings.Set(1, 2);
                tp.Settings.Set(2, 1);
                tp.Settings.Set(4, 1);
                tp.Info.TotalExp = 6;
                tp.Info.loginMap = 10017; //ship map 10017;
                tp.Info.X = 462; // ship x 1042;
                tp.Info.Y = 1255; //ship y 1075;
                tp.Gold = 0;



                //tp.Info.MySkills.AddSkill(cGlobal.SkillManager.GetSkillByID(15003));
                //create a character id for this new character
                //tp.Info.ID = tp.UserID + (byte)(tp.Slot - 1);
                // cGlobal.GameDB.CreateInventory((sender as WloPlayer));

                if (!tp.isConnected)
                {
                    tp.Slot = 0;
                    SendPacket p = new SendPacket();
                    p.Header(0, 31);
                    tp.Send(p);
                }
                else
                {
                    var id = tp.CharacterID;
                    if (cGlobal.CharacterDB.WriteNewPlayer(tp.CharacterID, tp.DBData(), (byte)tp.Info.Potential, tp.Eqs.Stat_toSave, null, tp.Eqs.EqData))
                        switch (tp.UserLoc)
                        {
                            case UserDBLoc.WLO4EVER:
                                {
                                    cGlobal.WLO4EVERDB.Update_Player_ID(tp.DbuserID, tp.CharacterID, tp.Slot);
                                    cGlobal.WLO4EVERDB.UpdateUser(tp.DbuserID, tp.char_delete_code);
                                } break;
                            case UserDBLoc.TEST:
                                {
                                    cGlobal.TestDB.Update_Player_ID(tp.DbuserID, tp.CharacterID, tp.Slot);
                                    cGlobal.TestDB.UpdateUser(tp.DbuserID, tp.char_delete_code);
                                } break;
                        }


                    //tp.CharacterState = GameCharacter.PlayerState.Logging_In;
                    NormalLog(tp);
                }
            }
            catch
            {
                SendPacket p = new SendPacket();
                p.Header(0, 49);
                tp.Send(p);
            }
        }

        void Recv2(ref Player tp, RecvPacket e)
        {
            int nameLen = e.Size - 2;
            string name = e.GetStringRaw(2, nameLen);
            if ((nameLen < 4) || (nameLen > 14) || !cGlobal.CharacterDB.LockName(name) || !tp.isConnected)
            {
                SendPacket p = new SendPacket();
                p.Header(9, 3);
                p.AddByte(1);
                tp.Send(p);
                return;
            }
            tp.Info.Name = name;
            SendPacket qp = new SendPacket();
            qp.Header(9, 3);
            qp.AddByte(0);
            tp.Send(qp);
        }


        cItem[] GetNoobClothes(Player p)
        {
            cItem[] y = new cItem[1];
            switch (p.Info.Body)
            {
                case BodyStyle.Big_Female:
                    {
                        HairStyle_BigF g = new HairStyle_BigF();
                        switch (Enum.GetName(g.GetType(), (HairStyle_BigF)p.Info.Head))
                        {
                            case "Iris":
                                {
                                    y = new cItem[]{ cGlobal.WloItemManager.GetItem(22005),
                                       cGlobal.WloItemManager.GetItem(21006),cGlobal.WloItemManager.GetItem(23001),
                                       cGlobal.WloItemManager.GetItem(24006)};
                                } break;
                            case "Lique":
                                {
                                    y = new cItem[]{ cGlobal.WloItemManager.GetItem(22009),
                                       cGlobal.WloItemManager.GetItem(21014),cGlobal.WloItemManager.GetItem(18002),
                                       cGlobal.WloItemManager.GetItem(24014)};
                                } break;
                            case "Maria":
                                {
                                    y = new cItem[]{ cGlobal.WloItemManager.GetItem(22006),
                                       cGlobal.WloItemManager.GetItem(21011),cGlobal.WloItemManager.GetItem(10004),
                                       cGlobal.WloItemManager.GetItem(24011)};
                                } break;
                            case "Vanessa":
                                {
                                    y = new cItem[]{ cGlobal.WloItemManager.GetItem(21015),
                                       cGlobal.WloItemManager.GetItem(24008)};
                                } break;
                            case "Breillat":
                                {
                                    y = new cItem[]{ cGlobal.WloItemManager.GetItem(22007),
                                       cGlobal.WloItemManager.GetItem(21009),cGlobal.WloItemManager.GetItem(10002),
                                       cGlobal.WloItemManager.GetItem(24009)};
                                } break;
                            case "Karin":
                                {
                                    y = new cItem[]{ cGlobal.WloItemManager.GetItem(21015),
                                        cGlobal.WloItemManager.GetItem(22008),
                                       cGlobal.WloItemManager.GetItem(24015)};
                                } break;
                            case "Konnotsuroko":
                                {
                                    y = new cItem[]{ cGlobal.WloItemManager.GetItem(24013),
                                       cGlobal.WloItemManager.GetItem(21013),};
                                } break;
                            case "Jessica":
                                {
                                    y = new cItem[]{ cGlobal.WloItemManager.GetItem(22002),
                                       cGlobal.WloItemManager.GetItem(21010),cGlobal.WloItemManager.GetItem(10003),
                                       cGlobal.WloItemManager.GetItem(24010)};
                                } break;
                        }
                    } break;
                case BodyStyle.Big_Male:
                    {
                        HairStyle_BigM g = new HairStyle_BigM();
                        switch (Enum.GetName(g.GetType(), (HairStyle_BigM)p.Info.Head))
                        {
                            case "Daniel":
                                {
                                    y = new cItem[]{ cGlobal.WloItemManager.GetItem(21004),
                                       cGlobal.WloItemManager.GetItem(24004)};
                                } break;
                            case "Sid":
                                {
                                    y = new cItem[]{ cGlobal.WloItemManager.GetItem(21005),
                                       cGlobal.WloItemManager.GetItem(24005)};
                                } break;
                            case "More":
                                {
                                    y = new cItem[]{ cGlobal.WloItemManager.GetItem(21012),
                                       cGlobal.WloItemManager.GetItem(24012)};
                                } break;
                            case "Kurogane":
                                {
                                    y = new cItem[]{ cGlobal.WloItemManager.GetItem(22009),
                                       cGlobal.WloItemManager.GetItem(21014),cGlobal.WloItemManager.GetItem(18002),
                                       cGlobal.WloItemManager.GetItem(24014)};
                                } break;
                        }
                    } break;
                case BodyStyle.Small_Female:
                    {
                        HairStyle_SmallF g = new HairStyle_SmallF();
                        switch (Enum.GetName(g.GetType(), (HairStyle_SmallF)p.Info.Head))
                        {
                            case "Nina":
                                {
                                    y = new cItem[]{ cGlobal.WloItemManager.GetItem(22003),
                                       cGlobal.WloItemManager.GetItem(21002),cGlobal.WloItemManager.GetItem(24002)};
                                } break;
                            case "Betty":
                                {
                                    y = new cItem[]{ cGlobal.WloItemManager.GetItem(22001),
                                       cGlobal.WloItemManager.GetItem(21003),cGlobal.WloItemManager.GetItem(24003)};
                                } break;
                        }
                    } break;
                case BodyStyle.Small_Male:
                    {
                        HairStyle_SmallM g = new HairStyle_SmallM();
                        switch (Enum.GetName(g.GetType(), (HairStyle_SmallM)p.Info.Head))
                        {
                            case "Rocco":
                                {
                                    y = new cItem[]{ cGlobal.WloItemManager.GetItem(21001),
                                       cGlobal.WloItemManager.GetItem(24001)};
                                } break;
                        }

                    } break;
            }
            return y;
        }


        #region wlo methods
        public byte[] Get_63_1Data(Character player, byte slot)
        {
            SendPacket temp = new SendPacket();
            temp.AddByte(slot);// data[at] = slot; at++;//PackSend->AddByte(1);
            temp.AddString(player.Name);// data[at] = nameLen; at++;
            temp.AddByte((byte)player.Level);// data[at] = level; at++;//	PackSend->AddByte(tmp1.level);					// Level 
            temp.AddByte((byte)player.Element);// data[at] = element; at++;//	PackSend->AddByte(3);  					// element
            temp.AddDWord((uint)player.FullHP);// putDWord(maxHP, data + at); at += 4;//	PackSend->AddDWord(tmp1.maxHP); 			// max hp
            temp.AddDWord((uint)player.CurHP);// putDWord(curHP, data + at); at += 4;//	PackSend->AddDWord(tmp1.curHP); 			// cur hp
            temp.AddDWord((uint)player.FullSP);// putDWord(maxSP, data + at); at += 4;//	PackSend->AddDWord(tmp1.maxSP); 			// max sp
            temp.AddDWord((uint)player.CurSP);// putDWord(curSP, data + at); at += 4;//	PackSend->AddDWord(tmp1.curSP); 			// cur sp
            temp.AddDWord(player.TotalExp);// putDWord(experience, data + at); at += 4;//	PackSend->AddDWord(tmp1.exp);			// exp
            temp.AddDWord(player.Gold);// putDWord(gold, data + at); at += 4;//	PackSend->AddDWord(tmp1.gold); 			// gold
            temp.AddByte((byte)player.Body);// data[at] = body; at++;//	PackSend->AddByte(tmp1.body); 					// body style
            temp.AddByte(0);
            temp.AddByte(player.Head);
            temp.AddByte(0);// data[at] = 0; data[at + 1] = head; data[at + 2] = 0; at += 3;//	PackSend->AddArray(tmp1.hair,3);// hair style
            temp.AddWord(player.HairColor);// putDWord(color1, data + at); at += 4;//	PackSend->AddDWord(tmp1.colors1);
            temp.AddWord(player.SkinColor);
            temp.AddWord(player.ClothingColor);
            temp.AddWord(player.EyeColor);
            temp.AddByte(BitConverter.GetBytes(player.Reborn)[0]);
            temp.AddByte((byte)player.Job);// data[at] = rebirth; data[at + 1] = job; at += 2;//PackSend->AddByte(tmp1.rebirth);PackSend->AddByte(tmp1.rebirthJob); 				// rebirth flag, job skill
            for (byte a = 1; a < 7; a++)
                temp.AddWord(player[a].ItemID);

            if (temp.PacketSize < 1) return null;
            return temp.Data;
        }
        public void NormalLog(Player c)
        {
            //c.CharacterState = GameCharacter.PlayerState.Logging_In;
            //a connection request was recieved
            c.DataOut = SendType.Multi;
            SendPacket p = new SendPacket();
            p.Header(20, 8);
            c.Send(p);

            Send_24_5(c, 183);
            Send_24_5(c, 53);
            Send_24_5(c, 52);
            Send_24_5(c, 54);
            Send_70_1(ref c, 23, "Something", 194);
            p = new SendPacket();
            p.Header(20, 33);
            p.AddByte(0);
            c.Send(p);
            //--------------------------Player PreInfo
            c.Info.Send8_1();
            p = new SendPacket();
            p.Header(14, 13);
            p.AddByte(3);
            c.Send(p);
            //------------------------Im Mall List
            //g.ac75.Send_1(g.gImMall_Manager.Get_75IM);
            p = new SendPacket();
            p.Header(75, 8);
            p.AddWord(0);
            c.Send(p);
            //------------------------
            c.Send_3_Me();
            cGlobal.World.SendCurrentPlayers(c);
            //send sidebar
            c.Send_5_3();
            c.Send_Inventory();
            p = new SendPacket();
            p.Header(23, 11);
            p.AddArray(c.Eqs._23_11Data);
            c.Send(p);
            SendPacket g = new SendPacket();
            //    g.Header(24, 6);
            //    g.AddArray(new byte[] { 001, 008, 047, 001, 002, 244, 050, 001, 003, 012, 043, 001 });
            //    g.SetSize();
            //    Send(g);
            //    g = new SPacket();
            //    g.Header(53, 10);
            //    g.AddArray(new byte[] { 032, 164, 036, 002, 037, 240, 038, 041, 058, 048, 083, 015 });
            //    g.SetSize();
            //    Send(g);
            //    g = new SPacket();
            //    g.Header(26, 7);
            //    g.AddArray(new byte[] { 001, 002, 002, 128, 003, 002, 004, 128 ,008,
            //                066, 009, 096, 010, 008, 011, 010 ,013, 001 });
            //    g.SetSize();
            //    Send(g);
            p = new SendPacket();
            p.Header(26, 4);
            p.AddDWord(c.Gold);
            c.Send(p);
            p = new SendPacket();
            p.Header(33, 2);
            p.AddArray(c.Settings.Data);
            c.Send(p);
            c.UpdateClient_FriendList();

            //pets
            //-----------------------------------   
            //---------Warp Info---------------------------------------------------
            //put me in my maps list
            if (!cGlobal.World.Player_login(c.Info.loginMap, ref c))
            {
                p = new SendPacket();
                p.Header(1, 7);
                c.Send(p);
                return;
            }


            p = new SendPacket();
            p.Header(5, 15);
            p.AddByte(0);
            c.Send(p);
            p = new SendPacket();
            p.Header(62, 53);
            p.AddWord(2);
            c.Send(p);
            p = new SendPacket();
            p.Header(5, 21);
            p.AddByte(c.Slot);//hmmmmm
            c.Send(p);
            p = new SendPacket();
            p.Header(5, 11);
            p.AddDWord(15085);//hmmmmm
            p.AddWord(5000);
            c.Send(p);
            //g.ac5.Send_11(15085, 0);//244, 68, 8, 0, 5, 11, 237, 58, 0, 0, 0, 0,         

            //---------------------------------

            //g.ac62.Send_4(g.packet.cCharacter.cCharacterID); //tent items

            //--------------------------------------
            p = new SendPacket();
            p.Header(5, 14);
            p.AddByte(2);
            c.Send(p);
            p = new SendPacket();
            p.Header(5, 16);
            p.AddByte(2);
            c.Send(p);
            var time = DateTime.Now.ToOADate();
            Send_23_140(ref c, 3, time);
            Send_25_44(ref c, 1, time);
            //g.ac23.Send_106(1, 1);
            Send_SingleByte_AC(ref c, 23, 160, (byte)3);
            Send_SingleByte_AC(ref c, 75, 7, (byte)1);
            Send_57("", ref c);
            p = new SendPacket();
            p.Header(69, 1);
            p.AddByte(71);
            c.Send(p);
            p = new SendPacket();
            p.Header(20, 60);
            p.AddByte(1);
            c.Send(p);
            p = new SendPacket();
            p.Header(66, 1);
            p.AddArray(new byte[] { 001, 012, 043, 000, 000, 000, 000, 000, 000, 000, 000 });
            c.Send(p);
            for (byte a = 1; a < 11; a++)
                Send_5_13(ref c, a, 0);
            for (byte a = 1; a < 11; a++)
                Send_5_24(ref c, a, 0);
            p = new SendPacket();
            p.Header(23, 162);
            p.AddByte(2);
            p.AddWord(0);
            c.Send(p);
            p = new SendPacket();
            p.Header(26, 10);
            p.AddDWord(0);
            c.Send(p);
            Send_SingleByte_AC(ref c, 23, 204, (ushort)1);
            p = new SendPacket();
            p.Header(23, 208);
            p.AddByte(2);
            p.AddByte(3);
            p.AddDWord(0);
            c.Send(p);
            p = new SendPacket();
            p.Header(23, 208);
            p.AddByte(2);
            p.AddByte(4);
            p.AddDWord(0);
            c.Send(p);
            //g.ac23.Send_208(2, 3, 0);
            //g.ac23.Send_208(2, 4, 0);
            p = new SendPacket();
            p.Header(1, 11);
            c.Send(p);
            p = new SendPacket();
            p.Header(15, 19);
            p.AddByte(0);
            c.Send(p);

            //--------------------------------
            /*p = new SendPacket();
            p.Header(20, 33);
            p.AddByte(0);
            p.SetSize();
            c.Send(p);*/
            /* p = new SendPacket();
             p.Header(54);
             p.AddArray(
             new byte[] {
                 89,2,2,90,2,1,91,2,1,189,2,2,190,2,1,191,2,1});
             p.SetSize();
             c.Send(p);*/
            c.UpdateClientIM();// IM Points 
            p = new SendPacket();
            p.Header(90, 1);
            p.AddArray(new byte[] { 000, 007, 008, 002, 010, 002 });
            c.Send(p);
            p = new SendPacket();
            p.Header(5, 4);
            c.Send(p);
            c.DataOut = SendType.Normal;
            //c.CharacterState = GameCharacter.PlayerState.inMap;
            //---------------------------------
        }


        public void Send_SingleByte_AC(ref Player player, byte ac, byte subac, object byteval)
        {
            SendPacket p = new SendPacket();
            p.Header(ac, subac);
            if (byteval is byte)
            {
                p.AddByte((byte)byteval);
            }
            else if (byteval is ushort)
            {
                p.AddWord((ushort)byteval);
            }
            else if (byteval is UInt32)
            {
                p.AddDWord((UInt32)byteval);
            }
            player.Send(p);

        }
        public void Send_SingleByte_AC(ref Player player, byte ac, object byteval)
        {
            SendPacket p = new SendPacket();
            p.Header(ac);
            if (byteval is byte)
            {
                p.AddByte((byte)byteval);
            }
            else if (byteval is ushort)
            {
                p.AddWord((ushort)byteval);
            }
            player.Send(p);

        }
        public void Send_57(string text, ref Player o) //sends sytem prompt message
        {
            SendPacket p = new SendPacket();
            p.Header(23, 57);
            p.AddByte(152);
            p.AddString(text, false);
            o.Send(p);
        }
        public void Send_24_5(Player player, byte value)
        {
            SendPacket p = new SendPacket();
            p.Header(24, 5);
            p.AddByte(value);
            p.AddWord(0);
            player.Send(p);

        }
        public void Send_70_1(ref Player player, byte value, string name, UInt16 id)
        {
            SendPacket p = new SendPacket();
            p.Header(70, 1);
            p.AddByte(value);
            p.AddString(name);
            p.AddWord(id);
            p.AddByte(0);
            player.Send(p);

        }
        public void Send_23_140(ref Player player, byte val, Double t)//time related
        {
            SendPacket p = new SendPacket();
            p.Header(23, 140);
            p.AddByte(val);
            p.AddDouble(t);
            player.Send(p);
        }
        public void Send_25_44(ref Player player, byte val, double v)//time related
        {
            SendPacket p = new SendPacket();
            p.Header(25, 44);
            p.AddByte(val);
            p.AddDouble(v);
            player.Send(p);
        }
        public void Send_5_13(ref Player player, byte value, UInt16 wVal)
        {
            SendPacket p = new SendPacket();
            p.Header(5, 13);
            p.AddByte(value);
            p.AddWord(wVal);
            player.Send(p);
        }
        public void Send_5_24(ref Player player, byte Val, UInt16 wVal)
        {
            SendPacket p = new SendPacket();
            p.Header(5, 24);
            p.AddByte(Val);
            p.AddWord(wVal);
            player.Send(p);
        }
        #endregion
    }
}
