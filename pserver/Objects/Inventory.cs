﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using pserver.Network;

namespace pserver
{
    public class InventoryManager
    {
        private Dictionary<byte,InvItemCell> Items;
        public event onPacketSend onDataOut;

        public InventoryManager()
        {
            Items = new Dictionary<byte, InvItemCell>();
             for (byte a = 1; a < 51; a++)
               Items.Add(a, new InvItemCell());
        }

        #region IDictionary Members
        /// <summary>
        /// Used to check if an item exists in the list
        /// </summary>
        /// <param name="ItemID"></param>
        /// <param name="slot"></param>
        /// <returns>true if exists and returns the first slot location of the item</returns>
        public bool ContainsItem(ushort ItemID, out byte slot)
        {
            for (byte a = 1; a < Items.Count; a++)
            {
                if (Items[a].ItemID == ItemID)
                {
                    slot = a;
                    return true;
                }
            }
            slot = 0;
            return false;
        }

        public void RemoveAll()
        {
            for(byte a = 1;a<51;a++)
                if(Items[a].ItemID > 0)
                RemoveItem(a, Items[a].Ammt);
        }

        public InvItemCell RemoveItem(byte at, byte ammt, bool senddata = true)
        {
            InvItemCell remItem = new InvItemCell();
            remItem.CopyFrom(Items[at]);
            if (remItem.ItemID > 0)
            {
                var matrix = cGlobal.NumbertoMatrix(at);

                for (byte h = 0; h < remItem.InvHeight; h++)
                    for (byte w = 0; w < remItem.InvWidth; w++)
                    {
                        byte slot = (byte)cGlobal.MatrixtoNumber(matrix[0] + h, matrix[1] + w);
                        if (w == 0 && h == 0)
                        {
                            if (Items[slot].ParentSlot == 0 && (Items[at].Ammt == 1 || Items[at].Ammt - ammt == 0))
                            {
                                Items[slot].Clear(); continue;
                            }
                            else
                                Items[slot].Ammt -= ammt;
                        }
                        else
                            Items[slot].Clear();
                    }
                if (senddata)
                {
                    SendPacket p = new SendPacket();
                    p.Header(23, 9);
                    p.AddByte(at);
                    p.AddByte(ammt);
                    if (onDataOut != null) onDataOut(p);
                }
                return remItem;
            }
            return null;
        }

        public void AddItem(InvItemCell item,bool sendData = true)
        {
            if (item.ItemID == 0) return;

            int addammt = item.Ammt;
            int totalammt = 0;

            for (byte a = 1; a < 51; a++)
            {
                SendPacket tmp = new SendPacket();
                tmp.Header(23, 6);
                tmp.AddWord(item.ItemID);
                byte ammt = 0;
                for (int b = 0; b < addammt; b++)
                    if (CanPlace(a, item))
                    {
                        var matrix = cGlobal.NumbertoMatrix(a);
                        for (byte h = 0; h < item.InvHeight; h++)
                            for (byte w = 0; w < item.InvWidth; w++)
                            {
                                byte slot = (byte)cGlobal.MatrixtoNumber(matrix[0] + h, matrix[1] + w);
                                if (w == 0 && h == 0)
                                {
                                    if (Items[slot].ItemID == 0 && Items[slot].ParentSlot == 0)
                                    {
                                        Items[slot].CopyFrom(item);
                                        Items[slot].Ammt = 1;
                                    }
                                    else if (Items[slot].SpaceLeft == 0) goto end;
                                    else
                                        Items[slot].Ammt++;
                                    ammt++;
                                    totalammt++;
                                }
                                else
                                    Items[slot].ParentSlot = (byte)a;
                            }


                        //if (i.itemtype.ItemType == 39)
                        //    own.vechile.Add(i.ID, mast);
                    }
                    else
                        goto end;

            end:
                if (ammt > 0)
                {
                    addammt -= ammt;
                    tmp.AddByte(ammt);
                    tmp.AddDWord(0);
                    tmp.AddDWord(0);
                    tmp.AddDWord(0);
                    tmp.AddDWord(0);
                    tmp.AddDWord(0);
                    tmp.AddDWord(0);
                    tmp.AddWord(0);

                    if (sendData && onDataOut != null)
                        onDataOut(tmp);
                }
                if (totalammt == item.Ammt)
                    break;
            }
        }
        public int AddItem(InvItemCell item, byte at, byte pammt)
        {
            if (item.ItemID == 0) return 0;

            int addammt = pammt;
            int totalammt = 0;

                byte ammt = 0;
                for (int b = 0; b < addammt; b++)
                    if (CanPlace(at, item))
                    {
                        var matrix = cGlobal.NumbertoMatrix(at);
                        for (byte h = 0; h < item.InvHeight; h++)
                            for (byte w = 0; w < item.InvWidth; w++)
                            {
                                byte slot = (byte)cGlobal.MatrixtoNumber(matrix[0] + h, matrix[1] + w);
                                if (w == 0 && h == 0)
                                {
                                    if (Items[slot].ItemID == 0 && Items[slot].ParentSlot == 0)
                                    {
                                        Items[slot].CopyFrom(item);
                                        Items[slot].Ammt = 1;
                                    }
                                    else
                                        Items[slot].Ammt++;
                                    ammt++;
                                    totalammt++;
                                }
                                else
                                    Items[slot].ParentSlot = (byte)at;
                            }


                        //if (i.itemtype.ItemType == 39)
                        //    own.vechile.Add(i.ID, mast);
                    }
                    else
                        goto end;
            end:
                return ammt;            
        }

        public void MoveItem(byte from, byte to, byte ammt)
        {
            if (Items[from].ItemID == 0 || from == to) return;
            
            SendPacket tmp = new SendPacket();
            tmp.Header(23, 10);
            tmp.AddByte(from);

            var item = RemoveItem(from, ammt, false);

            if (item != null && (Items[to].ItemID == 0 || (Items[to].ItemID == item.ItemID)))
            {
                byte wasplaced = (byte)AddItem(item, to,ammt);
                if (wasplaced > 0)
                {
                    tmp.AddByte(wasplaced);
                    tmp.AddByte(to);
                    if (onDataOut != null) onDataOut(tmp);
                }
            }
        }

        bool CanPlace(byte cell, InvItemCell item)
        {
            try
            {
                var matrix = cGlobal.NumbertoMatrix(cell);
                for (int s = 0; s < item.InvHeight; s++)
                    for (int y = 0; y < item.InvWidth; y++)
                    {
                        var chk = (byte)cGlobal.MatrixtoNumber(matrix[0] + s, matrix[1] + y);
                        if (Items[chk].ItemID == 0 && Items[chk].ParentSlot == 0) continue;
                        else if (matrix[0] + (item.InvHeight - 1) > 51 && matrix[1] + (item.InvWidth - 1) > 6) return false;
                        else if (Items[chk].ItemID != 0 && Items[chk].ParentSlot != 0) return false;
                    }
            }
            catch (Exception e) { DLogger.ErrorLog(e.StackTrace, e.Message); return false; }
            return true;
        }

        public InvItemCell this[byte key]
        {
            get
            {
                // If this key is in the dictionary, return its value.
                if (Items.ContainsKey(key))
                {
                    // The key was found; return its value. 
                    return Items[key];
                }
                else
                {
                    // The key was not found; return null. 
                    throw new Exception("No key in Dictionary");
                }
            }

            set
            {
                // If this key is in the dictionary, change its value. 
                if (Items.ContainsKey(key))
                {
                    // The key was found; change its value.
                    Items[key] = value;
                }
                else
                {
                    throw new Exception("No key in Dictionary");
                }
            }
        }
        #endregion

        #region ICollection Members
        public int FilledCount { get { return Items.Count(c=>c.Value.ItemID >0); } }
        public int unFilledCount { get { return 50 - FilledCount; } }
        #endregion

        #region Wlo Methods
        public byte[] _23_5Data
        {
            get
            {
                byte[] data = null;
                if (FilledCount > 0)
                {
                    data = new byte[FilledCount * 29];
                    byte at = 0;
                    for (byte a = 1; a < Items.Count; a++)
                        if (Items[a].ItemID != 0)
                        {
                            data[at] = (byte)a; at++;
                            cGlobal.InsertWord(Items[a].ItemID, ref data, at); at += 2;
                            data[at] = (byte)Items[a].Ammt; at++;
                            data[at] = (byte)Items[a].Damage; at++;
                            for (int z = 0; z < 24; z++)
                            { data[at] = 0; at++; }
                        }
                }
                return data;
            }
        }
        #endregion
    }
}
