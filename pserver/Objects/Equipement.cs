﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using pserver.Network;

namespace pserver
{
    /// <summary>
    /// This will contain the stats for the character
    /// </summary>
    public class EquipementManager
    {
        Dictionary<byte, EquipmentCell> clothes;
        protected event onPacketSend onDataOut;

        public EquipmentCell this[byte key]
        {
            get
            {
                // If this key is in the dictionary, return its value.
                if (clothes.ContainsKey(key))
                {
                    // The key was found; return its value. 
                    return clothes[key];
                }
                else
                {
                    // The key was not found; return null. 
                    throw new Exception("No key in Dictionary");
                }
            }

            set
            {
                // If this key is in the dictionary, change its value. 
                if (clothes.ContainsKey(key))
                {
                    // The key was found; change its value.
                    clothes[key] = value;
                }
                else
                {
                    throw new Exception("No key in Dictionary");
                }
            }
        }

        public EquipementManager()
        {
            clothes = new Dictionary<byte, EquipmentCell>();
            for (byte a = 1; a < 7; a++)
                clothes.Add(a, new EquipmentCell());
        }
        

        public UInt16 SkillPoints { get; set; }
        uint m_totalexp = 6;
        public UInt32 TotalExp
        {
            get { return m_totalexp; }
            set
            {
                m_totalexp = value;

                uint tmp = m_totalexp;
                int level = 1;
                uint exp = (uint)CalcMaxExp(BitConverter.GetBytes(Reborn)[0], level);

                while (tmp > exp)
                {
                    tmp -= exp;
                    exp = (uint)CalcMaxExp(BitConverter.GetBytes(Reborn)[0], ++level);
                }
                m_currexp = (int)tmp;
            }
        }
        
        public bool Reborn { get { return (Job != RebornJob.none); } }
        public uint Gold;
        public byte Level // Level Client Sees
        {
            get
            {
                int level = 1;

                uint tmp = m_totalexp;
                uint exp = (uint)CalcMaxExp(BitConverter.GetBytes(Reborn)[0], level);

                while (tmp > exp)
                {
                    tmp -= exp;
                    exp = (uint)CalcMaxExp(BitConverter.GetBytes(Reborn)[0], ++level);
                }

                if (Reborn) return (byte)(level - 100);
                else
                    return (byte)level;
            }
        }

        public Int32 CurHP { get; set; }
        public Int32 CurSP { get; set; }

        public UInt32 ExptoLevel
        {
            get
            {
                return (uint)CalcMaxExp(BitConverter.GetBytes(Reborn)[0], Level);
            }
        }
        int m_currexp;
        public Int32 CurExp
        {
            get
            {
                return m_currexp;
            }
            set
            {
                TotalExp += (UInt32)(value - CurExp);
                CurExp = value;

                if(CurExp >= ExptoLevel)
                {
                    m_currexp -= (int)ExptoLevel;
                    SkillPoints += 5;
                    Send8_1(true);
                }
                else
                    Send_1(36, TotalExp, 0);

            }
        }

        public UInt16 Potential { get; set; }

        public byte Head;
        public BodyStyle Body;
        public ElementType Element { get; set; }
        public RebornJob Job { get; set; }

        //Base Calculated Stats of the character set by 9,1
        #region Base Stats
        UInt16 baseStr;
        public UInt16 Str
        {
            get
            {
                int add = 0;
                if (Body == BodyStyle.Big_Female && (HairStyle_BigF)Head == HairStyle_BigF.Iris)
                    add = 1;
                else if (Body == BodyStyle.Big_Female && (HairStyle_BigF)Head == HairStyle_BigF.Lique)
                    add = 2;
                else if (Body == BodyStyle.Big_Male && (HairStyle_BigM)Head == HairStyle_BigM.Kurogane)
                    add = 1;
                else if (Body == BodyStyle.Big_Male && (HairStyle_BigM)Head == HairStyle_BigM.Daniel)
                    add = 1;
                else if (Body == BodyStyle.Big_Male && (HairStyle_BigM)Head == HairStyle_BigM.Sid)
                    add = 2;

                return (ushort)(baseStr + (Level * add));
            }
            set { baseStr = value; }
        }
        UInt16 baseInt;
        public UInt16 Int
        {
            get
            {
                int add = 0;

                if (Body == BodyStyle.Big_Female && (HairStyle_BigF)Head == HairStyle_BigF.Vanessa)
                    add = 3;
                else if (Body == BodyStyle.Big_Female && (HairStyle_BigF)Head == HairStyle_BigF.Karin)
                    add = 1;
                else if (Body == BodyStyle.Big_Female && (HairStyle_BigF)Head == HairStyle_BigF.Jessica)
                    add = 1;
                else if (Body == BodyStyle.Big_Male && (HairStyle_BigM)Head == HairStyle_BigM.Kurogane)
                    add = 1;
                else if (Body == BodyStyle.Small_Female && (HairStyle_SmallF)Head == HairStyle_SmallF.Betty)
                    add = 1;
                else if (Body == BodyStyle.Small_Female && (HairStyle_SmallF)Head == HairStyle_SmallF.Nina)
                    add = 1;
                else if (Body == BodyStyle.Big_Female && (HairStyle_BigF)Head == HairStyle_BigF.Maria)
                    add = 2;
                else if (Body == BodyStyle.Small_Male && (HairStyle_SmallM)Head == HairStyle_SmallM.Rocco)
                    add = 2;
                else if (Body == BodyStyle.Big_Female && (HairStyle_BigF)Head == HairStyle_BigF.Konnotsuroko)
                    add = 2;
                return (ushort)(baseInt + (Level * add));

            }
            set { baseInt = value; }
        }
        UInt16 baseWis;
        public UInt16 Wis
        {
            get
            {
                int add = 0;

                if (Body == BodyStyle.Big_Female && (HairStyle_BigF)Head == HairStyle_BigF.Karin)
                    add = 1;
                else if (Body == BodyStyle.Big_Female && (HairStyle_BigF)Head == HairStyle_BigF.Jessica)
                    add = 2;
                else if (Body == BodyStyle.Big_Male && (HairStyle_BigM)Head == HairStyle_BigM.More)
                    add = 2;
                else if (Body == BodyStyle.Small_Female && (HairStyle_SmallF)Head == HairStyle_SmallF.Nina)
                    add = 1;
                else if (Body == BodyStyle.Big_Female && (HairStyle_BigF)Head == HairStyle_BigF.Konnotsuroko)
                    add = 1;
                return (ushort)(baseWis + (Level * add));
            }
            set { baseWis = value; }
        }
        UInt16 baseCon;
        public UInt16 Con
        {
            get
            {
                int add = 0;
                if (Body == BodyStyle.Big_Female && (HairStyle_BigF)Head == HairStyle_BigF.Iris)
                    add = 2;
                else if (Body == BodyStyle.Big_Male && (HairStyle_BigM)Head == HairStyle_BigM.Daniel)
                    add = 2;
                else if (Body == BodyStyle.Big_Male && (HairStyle_BigM)Head == HairStyle_BigM.Kurogane)
                    add = 1;
                else if (Body == BodyStyle.Big_Male && (HairStyle_BigM)Head == HairStyle_BigM.Sid)
                    add = 1;
                return (ushort)(baseCon + (Level * add));
            }
            set { baseCon = value; }
        }
        UInt16 baseAgi;
        public UInt16 Agi
        {
            get
            {
                int add = 0;

                if (Body == BodyStyle.Big_Female && (HairStyle_BigF)Head == HairStyle_BigF.Karin)
                    add = 1;
                else if (Body == BodyStyle.Big_Female && (HairStyle_BigF)Head == HairStyle_BigF.Lique)
                    add = 1;
                else if (Body == BodyStyle.Small_Female && (HairStyle_SmallF)Head == HairStyle_SmallF.Betty)
                    add = 2;
                else if (Body == BodyStyle.Small_Female && (HairStyle_SmallF)Head == HairStyle_SmallF.Nina)
                    add = 1;
                else if (Body == BodyStyle.Big_Female && (HairStyle_BigF)Head == HairStyle_BigF.Maria)
                    add = 1;
                else if (Body == BodyStyle.Small_Male && (HairStyle_SmallM)Head == HairStyle_SmallM.Rocco)
                    add = 1;
                else if (Body == BodyStyle.Big_Male && (HairStyle_BigM)Head == HairStyle_BigM.More)
                    add = 1;
                return (ushort)(baseAgi + (Level * add));
            }
            set { baseAgi = value; }
        }
        #endregion
        
        #region Calculated Stats based on element and job variables
        UInt16 Atk
        {
            get
            {
                int add = 0;
                switch (Element)
                {
                    case ElementType.Fire:
                        add = (UInt16)(Math.Round(((double)Level * 2.0) + ((double)Str * 2.0)));
                        break;
                    default:
                        add = (UInt16)(Math.Round(((double)Level * 1.4) + ((double)Str * 2.0)));
                        break;
                }

                if (Job == RebornJob.Killer)
                    add = (UInt16)Math.Round((double)add * 1.1);

                return (ushort)add;
            }
        }
        UInt16 Matk

        {
            get
            {
                int add = 0;
                switch (Element)
                {
                    case ElementType.Fire:
                        add = (UInt16)(Math.Round(((double)Level * 1.6) + ((double)Int * 2.0)));
                        break;
                    default:
                        add = (UInt16)(Math.Round(((double)Level * 1.4) + ((double)Int * 2.0)));
                        break;
                }

                if (Job == RebornJob.Wit)
                    add = (UInt16)Math.Round((double)add * 1.1);

                return (ushort)add;

            }
            set { baseInt = value; }
        }
        UInt16 Mdef

        {
            get
            {
                int add = 0;
                switch (Element)
                {
                    case ElementType.Fire:
                        add = (UInt16)(Math.Round(((double)Level * 2.2) + ((double)Wis * 2.2)));
                        break;
                    default:
                        add = (UInt16)(Math.Round(((double)Level * 2.0) + ((double)Wis * 2.2)));
                        break;
                }

                if (Job == RebornJob.Priest)
                    add = (UInt16)Math.Round((double)add * 1.1);

                return (ushort)add;
            }
            set { baseWis = value; }
        }
        UInt16 Def

        {
            get
            {
                int add = 0;
                switch (Element)
                {
                    case ElementType.Earth:
                        add = (UInt16)(Math.Round(((double)Level * 8.0) + ((double)Con * 1.75)));
                        break;
                    default:
                        add = (UInt16)(Math.Round(((double)Level * 2.0) + ((double)Con * 1.75)));
                        break;
                }

                if (Job == RebornJob.Warrior)
                {
                    add = (UInt16)Math.Round((double)add * 1.1);
                }
                return (ushort)add;
            }
        }
        UInt16 Spd

        {
            get
            {
                int add = 0;
                switch (Element)
                {
                    case ElementType.Wind:
                        add = (UInt16)(Math.Round(((double)Level * 2.1) + ((double)Agi * 2.2)));
                        break;
                    default:
                        add = (UInt16)(Math.Round(((double)Level * 1.6) + ((double)Agi * 2.2)));
                        break;
                }

                if (Job == RebornJob.Knight || Job == RebornJob.Seer)
                    add = (UInt16)Math.Round((double)add * 1.1);

                return (ushort)add;
            }
        }
#endregion
        
        #region Full Stats
        public Int32 FullHP { get { return (int)(MaxHp + EquippedMaxHP); } }
        public Int32 FullSP { get { return (int)(MaxSp + EquippedMaxSP); } }
        public Int32 FullAtk { get { return (int)(Atk + EquippedATK); } }
        public Int32 FullDef { get { return (int)(Def + EquippedDEF); } }
        public Int32 FullMatk { get { return (int)(Matk + EquippedMAT); } }
        public Int32 FullMdef { get { return (int)(Mdef + EquippedMDF); } }
        public Int32 FullSpd { get { return (int)(Spd + EquippedSPD); } }
        #endregion

        //Base Calculated Max HP and SP
        UInt32 MaxHp
        {

            get { return (UInt32)(((Math.Round(Math.Pow((double)Level, 0.35) * Con) * 2) + (Level) + (Con * 2) + 180 + 0)); }

        }
        UInt16 MaxSp

        {
            get { return (UInt16)(((Math.Round(Math.Pow((double)Level, 0.3) * Wis) * 3.2) + Level + (Wis * 2) + 94 + 0)); }
        }

        #region Equipped Bonuses

        protected Int32 EquippedMaxHP
        {
            get
            {
                Int32 nRet = 0;
                for (byte i = 1; i < 7; ++i)
                    nRet += clothes[i].HP;
                return nRet;
            }
        }

        protected Int32 EquippedMaxSP
        {
            get
            {
                Int32 nRet = 0;
                for (byte i = 1; i < 7; ++i)
                    nRet += clothes[i].SP;
                return nRet;
            }
        }

        protected UInt16 EquippedATK
        {
            get
            {
                Int16 nRet = 0;
                for (byte i = 1; i < 7; ++i)
                    nRet += (Int16)clothes[i].ATK;
                return (UInt16)nRet;
            }
        }

        protected UInt16 EquippedDEF
        {
            get
            {
                Int16 nRet = 0;
                for (byte i = 1; i < 7; ++i)
                    nRet += (Int16)clothes[i].DEF;
                return (UInt16)nRet;
            }
        }

        protected UInt16 EquippedMAT
        {
            get
            {
                Int16 nRet = 0;
                for (byte i = 1; i < 7; ++i)
                    nRet += (Int16)clothes[i].MAT;
                return (UInt16)nRet;
            }
        }

        protected UInt16 EquippedMDF
        {
            get
            {
                Int16 nRet = 0;
                for (byte i = 1; i < 7; ++i)
                    nRet += (Int16)clothes[i].MDF;
                return (UInt16)nRet;
            }
        }

        protected UInt16 EquippedSPD
        {
            get
            {
                Int16 nRet = 0;
                for (byte i = 1; i < 7; ++i)
                    nRet += (Int16)clothes[i].SPD;
                return (UInt16)nRet;
            }
        }

        #endregion


        public byte[] Worn_Equips
        {
            get
            {
                byte[] ret = null;

                byte ammt = WornCount;
                if (ammt > 0)
                {
                    List<byte> tmp = new List<byte>();
                    for (byte a = 1; a < 7;a++ )
                        if (clothes[a].ItemID > 0)
                        {
                            byte[] data = new byte[2];
                            cGlobal.InsertWord(clothes[a].ItemID, ref data, 0);
                            tmp.AddRange(data);
                        }
                    ret = tmp.ToArray();
                }
                return ret;
            }
        }
        
        public byte[] FullEqData
        {
            get
            {

                uint at = 0;
                byte[] data = new byte[12];
                for (byte n = 1; n < 7; n++)
                    if (clothes[n].ItemID != 0)
                    { cGlobal.InsertWord(clothes[n].ItemID, ref data, at); at += 2; }
                    else
                    { cGlobal.InsertWord(0, ref data, at); at += 2; }

                return data;
            }
        }
        
        public byte[] _23_11Data
        {
            get
            {
                byte[] data = null;
                int ammt = WornCount;
                if (ammt > 0)
                {
                    data = new byte[ammt * 19];
                    uint at = 0;
                    for (byte n = 1; n < 7; n++)
                    {
                        if (clothes[n].ItemID != 0)
                        {
                            cGlobal.InsertWord(clothes[n].ItemID, ref data, at); at += 2;
                            data[at] = (byte)clothes[n].Damage; at++;
                            for (int z = 0; z < 16; z++)
                            { data[at] = 0; at++; }
                        }
                    }
                }
                return data;
            }
        }
        
        public byte WornCount
        {
            get
            {
                return (byte)clothes.Values.Count(c => c.ItemID > 0);
            }
        }


        #region Saving/Loading

        public List<uint[]> Stat_toSave
        {
            get
            {
                List<uint[]> tmp = new List<uint[]>();
                tmp.Add(new uint[] { 36, TotalExp, 0 });
                tmp.Add(new uint[] { 38, SkillPoints, 0 });
                tmp.Add(new uint[] { 25, (uint)CurHP, 0 });
                tmp.Add(new uint[] { 26, (uint)CurSP, 0 });
                tmp.Add(new uint[] { 28, baseStr, Potential });
                tmp.Add(new uint[] { 29, baseCon, Potential });
                tmp.Add(new uint[] { 30, baseAgi, Potential });
                tmp.Add(new uint[] { 27, baseInt, Potential });
                tmp.Add(new uint[] { 33, baseWis, Potential });
                return tmp;
            }
        }

        public Dictionary<byte, uint[]> EqData
        {
            get
            {
                Dictionary<byte, uint[]> tmp = new Dictionary<byte, uint[]>();
                for (byte a = 1; a < 7; a++)
                    tmp.Add(a, new uint[] { clothes[a].ItemID, clothes[a].Damage, clothes[a].Ammt, (uint)clothes[a].Data.EquipPos, 0, 0, 0, 0 });
                return tmp;
            }
        }
        
        public void LoadStats(List<uint[]> src)
       {
           try
           {
               foreach (var t in src)
               {
                   switch (t[0])
                   {
                       case 25: CurHP = (int)t[1]; break;
                       case 26: CurSP = (ushort)t[1]; break;
                       case 38: SkillPoints = (ushort)t[1]; break;
                       case 36: TotalExp = (uint)t[1]; break;
                       case 28: Str = (ushort)t[1]; break;
                       case 29: Con = (ushort)t[1]; break;
                       case 30: Agi = (ushort)t[1]; break;
                       case 27: Int = (ushort)t[1]; break;
                       case 33: Wis = (ushort)t[1]; break;
                   }
                   Potential = (byte)t[2];
               }
           }
           catch (Exception v)
           {
               DLogger.ErrorLog(v.StackTrace, v.Message);
           }
       }

        #endregion
        
        int CalcMaxExp(byte rebirth, int Level)
        {
            if (rebirth == 0)
            {
                return (int)Math.Round(Math.Pow((Level + 1), 3.1) + 5);
            }
            else
            {
                if (Level < 150)
                {
                    return (int)Math.Pow((double)(Level + 1), (3.3)) + 50;
                }
                else
                {
                    return (int)Math.Pow((Level + 1), (3.3)) + (int)Math.Pow((Level + 1 - 150), 4.9);
                }
            }
        }

        #region Stat Management
        void AddtoStat(byte statType, byte ammt)
        {
            if (SkillPoints == 0) return;
            SkillPoints -= ammt;

            switch (statType)
            {
                case 27://int
                    {
                        Int += ammt;
                        Send_1(27, Int, 0);
                        Send_1(43, Matk, 0);
                        Send_1(38, SkillPoints, 0);
                    } break;
                case 28://str
                    {
                        Str += ammt;
                        Send_1(28, Str, 0);
                        Send_1(41, Atk, 0);
                        Send_1(38, SkillPoints, 0);
                    } break;
                case 29://con
                    {
                        Con += ammt;
                        Send_1(29, Con, 0);
                        Send_1(42, Def, 0);
                        Send_1(205, (uint)FullHP, 0);
                        Send_1(38, SkillPoints, 0);
                    } break;
                case 30://agi
                    {
                        Agi += ammt;
                        Send_1(30, Agi, 0);
                        Send_1(45, Spd, 0);
                        Send_1(38, SkillPoints, 0);
                    } break;
                case 33://wis
                    {
                        Wis += ammt;
                        Send_1(33, Wis, 0);
                        Send_1(44, Mdef, 0);
                        Send_1(206, MaxSp, 0);
                        Send_1(38, SkillPoints, 0);
                    } break;
            }
        }

        void RemfromStat(byte statType, byte ammt)
        {
        }
        
        protected void Send_1(byte stat, UInt32 ammt, UInt32 skill)
        {
            SendPacket p = new SendPacket();
            p.Header(8, 1);
            p.AddByte(stat);
            p.AddByte(1);
            p.AddDWord(ammt);
            p.AddDWord(skill);
            if (onDataOut != null) onDataOut(p);
        }
        
        public void Send8_1(bool levelup = false) //this function sets the stas according to pts
        {

            if (levelup)
            {
                Send_1(36, TotalExp, 0);
                Send_1(35, Level, 0);
                Send_1(37, (uint)(Level - 1), 0);
                Send_1(38, SkillPoints, 0);
                CurHP = FullHP;
                CurSP = FullSP;
            }

            Send_1(207, (uint)EquippedMaxHP, 0); //a plus to hp
            Send_1(25, (uint)((CurHP > FullHP) ? FullHP : CurHP), 0);
            Send_1(208, (uint)EquippedMaxSP, 0); //a plus to sp
            Send_1(26, (uint)((CurSP > FullSP) ? FullSP : CurSP), 0);
            Send_1(210, (uint)EquippedATK, 0);
            Send_1(41, (uint)FullAtk, 0);
            Send_1(211, (uint)EquippedDEF, 0);
            Send_1(42, (uint)FullDef, 0);
            Send_1(214, (uint)EquippedSPD, 0);
            Send_1(45, (uint)FullSpd, 0);
            Send_1(215, (uint)EquippedMAT, 0);
            Send_1(43, (uint)FullMatk, 0);
            Send_1(216, (uint)EquippedMDF, 0);
            Send_1(44, (uint)FullMdef, 0);
        }
        #endregion

        
        
        public InvItemCell RemoveEQ(byte clothesSlot)
        {
            InvItemCell i = new InvItemCell();
            i.CopyFrom(clothes[clothesSlot]);
            clothes[clothesSlot].Clear();
            return i;
        }

        
        public InvItemCell SetEQ(byte clothesSlot, InvItemCell eq)
        {
            InvItemCell i = null;
            try
            {
                if (clothes[clothesSlot].ItemID != 0)
                    i = RemoveEQ(clothesSlot);
                clothes[clothesSlot].CopyFrom(eq);
            }
            catch { } return i;
        }


        void Send_16(byte src, byte dst) //deequipping
        {
            SendPacket p = new SendPacket();
            p.Header(23, 16);
            p.AddByte(src);
            p.AddByte(dst);
            if (onDataOut != null) onDataOut(p);
        }

        void Send_17(byte src, byte dst) //equipping
        {
            SendPacket p = new SendPacket();
            p.Header(23, 17);
            p.AddByte(src);
            p.AddByte(dst);
            if (onDataOut != null) onDataOut(p);
        }

    }
}
