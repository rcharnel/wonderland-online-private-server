﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace pserver
{
    public class WarpPortal
    {
        public ushort ID;
        public ushort DstMap;
        public ushort DstX_Axis;
        public ushort DstY_Axis;
    }
}
