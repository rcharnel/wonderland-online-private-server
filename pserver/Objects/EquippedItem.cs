﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace pserver
{
    public class EquipmentCell : cItem
    {
        public EquipmentCell()
        {
        }


        public Int32 HP
        {
            get
            {
                int val = 0;
                val += (Data.statType[0] == 207) ? (Int32)Data.statVal[0] : 0;
                val += (Data.statType[1] == 207) ? (Int32)Data.statVal[1] : 0;
                return val;
            }
        }

        public Int32 SP
        {
            get
            {
                int val = 0;
                val += (Data.statType[0] == 208) ? (Int32)Data.statVal[0] : 0;
                val += (Data.statType[1] == 208) ? (Int32)Data.statVal[1] : 0;
                return val;
            }
        }

        public Int32 ATK
        {
            get
            {
                int val = 0;
                val += (Data.statType[0] == 210) ? (Int32)Data.statVal[0] : 0;
                val += (Data.statType[1] == 210) ? (Int32)Data.statVal[1] : 0;
                return val;
            }
        }

        public Int32 DEF
        {
            get
            {
                int val = 0;
                val += (Data.statType[0] == 211) ? (Int32)Data.statVal[0] : 0;
                val += (Data.statType[1] == 211) ? (Int32)Data.statVal[1] : 0;
                return val;
            }
        }

        public Int32 MAT
        {
            get
            {
                int val = 0;
                val += (Data.statType[0] == 215) ? (Int32)Data.statVal[0] : 0;
                val += (Data.statType[1] == 215) ? (Int32)Data.statVal[1] : 0;
                return val;
            }
        }

        public Int32 MDF
        {
            get
            {
                int val = 0;
                val += (Data.statType[0] == 216) ? (Int32)Data.statVal[0] : 0;
                val += (Data.statType[1] == 216) ? (Int32)Data.statVal[1] : 0;
                return val;
            }
        }

        public Int32 SPD
        {
            get
            {
                int val = 0;
                val += (Data.statType[0] == 214) ? (Int32)Data.statVal[0] : 0;
                val += (Data.statType[1] == 214) ? (Int32)Data.statVal[1] : 0;
                return val;
            }
        }

        public Int32 Resist
        {
            get
            {
                int val = 0;
                return val;
            }
        }

        public Int32 Crit
        {
            get
            {
                return (Data.SpecialStatus == eSpecialStatus.Critical_Increase) ? Data.ItemRank * 2 + 10 : 0;
            }
        }

    }
}
