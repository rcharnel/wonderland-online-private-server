﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;
using pserver.Network;

namespace pserver
{
    public enum UserDBLoc
    {
        NONE,
        WLO4EVER,
        ILOVEWLO,
        TEST,
    }
    public class Player: Network.NetClient, System.ComponentModel.INotifyPropertyChanged
    {
        object mylock = new object();

        #region Timers
        public System.Diagnostics.Stopwatch onlinetimer;
        #endregion

        #region Events
        public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;
        public event EventHandler Disconnected;
        public event onPacketRecieve RecievedData;
        #endregion
        
        #region Managers
        public inGameSettings Settings;
        List<Character> Friends;
        List<Mail> MailBox;
        //public WarpInfo recordMap, gpsMap, lastMap;
        
        public InventoryManager Inv { get { return Info.Inventory ?? null; } }
        public EquipementManager Eqs { get { return ((EquipementManager)Info) ?? null; } }

        #endregion

        public bool iswarping;
                
        public Dictionary<int, Character> myCharlist;
        public Character Info = null;

        byte slot;
        public byte Slot
        {
            get { return slot; }
            set
            {
                if (slot != 0 || value == 0)
                {
                    Info.Inventory.onDataOut -= Event_onDataOut;
                    Info.DataOut -= Event_onDataOut;
                }
                slot = value;
                Info = myCharlist[slot];
                if (slot != 0)
                {                    
                    Info.Inventory.onDataOut += Event_onDataOut;
                    Info.DataOut += Event_onDataOut;
                }
            }
        }


        public uint DbuserID;
        public uint UserID
        {
            get
            {
                int add = 0;
                switch (UserLoc)
                {
                    case UserDBLoc.WLO4EVER: add += 2000; break;
                    case UserDBLoc.TEST: add += 1000; break;
                }
                return (uint)(DbuserID + add);
            }
        }

        public string userName;
        public UserDBLoc UserLoc;
        public string char_delete_code;
        
        public TimeSpan TimeOnline { get { try { return onlinetimer.Elapsed; } catch { return new TimeSpan(); } } }
        public TimeSpan TimeIdle { get; set; }

        public Player()
        {
            Settings = new inGameSettings();
            //gpsMap = lastMap = recordMap = new WarpInfo();

            myCharlist = new Dictionary<int,Character>();
            myCharlist.Add(1, new Character());
            myCharlist.Add(2, new Character());
            Friends = new List<Character>();
            Connection_Terminated += Player_Connection_Terminated;
            onlinetimer = new System.Diagnostics.Stopwatch();
        }
        
        #region Saving/Loading Use Only

        public string BodyString
        {
            get
            {
                string s = (byte)Info.Body + "," + Info.Head + "," + Info.HairColor + "," + Info.SkinColor + ","
                    + Info.ClothingColor + "," + Info.EyeColor;
                return s;
            }
        }

        public string[] DBData()
        {
            try
            {
                return new string[] {CharacterID.ToString(),Info.Head.ToString(),((byte)Info.Body).ToString(),CharacterName,Nickname,
                CurrentMap.MapID.ToString(),X_Axis.ToString(),Y_Axis.ToString(),Info.HairColor.ToString(),Info.SkinColor.ToString(),
               Info.ClothingColor.ToString(),Info.EyeColor.ToString(),Info.Gold.ToString(),((byte)Info.Element).ToString(),
               BitConverter.GetBytes(Info.Reborn)[0].ToString(),((byte)Info.Job).ToString()};
            }
            catch { return null; }
        }

       
        #endregion
                
        #region Stats

        public uint Gold { get { return Info.Gold; } set { Info.Gold = value; SendGold(); } }
        public Int32 CurHP
        {
            get { return Info.CurHP; }
            set
            {
                Info.CurHP = value;
                //Send_1(25, (uint)Info.CurHP, 0);
            }
        }
        public Int32 CurSP
        {
            get { return Info.CurSP; }
            set
            {
                Info.CurSP = value;
               // Send_1(26, (uint)Info.CurSP, 0);
            }
        }
        
        

        #region Stats Handler

        
        
        //public void ItemUsage(UInt16 statType, UInt16 ammt)
        //{
        //    switch (statType)
        //    {
        //        case 25://int
        //            {
        //                CurHP += ammt;
        //            } break;
        //        case 26://str
        //            {
        //                CurSP += ammt;
        //            } break;
        //    }
        //}
        #endregion
        #endregion

        #region Inventory

        //public void DropItem(byte src, byte ammt)
        //{
        //    //check to see if I even have that ammt in my inventory
        //    if ((src > 0) && (src < 51))
        //    {
        //        if (Info.Inventory[src].Ammt >= ammt)
        //        {
        //            cInvItem drop = new cInvItem();
        //            drop.CopyFrom(Info.Inventory[src]);
        //            drop.Ammt = ammt;
        //            //do a drop item on map
        //            //if (Owner.CurrentMap.ItemDropped(drop, Owner.PlayerREf))
        //            //{

        //            //    // if (i.Data.ItemType == 39)
        //            //    //own.vechile.Rem(src);
        //            //    RemoveItem(src, ammt, true);
        //            //}
        //        }
        //    }
        //}

        //public void PickupItem(UInt16 itemIndex)
        //{
        //    cGroundItem gi = new cGroundItem();
        //    //if (Owner.CurrentMap.HasGroundItem(itemIndex, out gi))
        //    //{
        //    //    cInvItem i = new cInvItem(Host);
        //    //    i.CopyFrom(gi);
        //    //    byte ammt = i.Ammt;
        //    //    if (PlaceItem(i, i.Ammt))
        //    //    {
        //    //        Owner.PlayerREf.DataOut = SendType.Multi;
        //    //        Owner.currentMap.PickupGroundItem(itemIndex, Host.FindPlayerby_CharacterID(Owner.CharacterID));
        //    //        Send_6(Owner.CharacterID, i, ammt);
        //    //        Owner.PlayerREf.DataOut = SendType.Normal;
        //    //    }
        //    //}

        //}

        //public void UseItem(byte slot, byte tgrt, byte ammt)
        //{
        //    //Get item first
        //    cInvItem i = Info.Inventory[slot];
        //    if (i.ID > 0)
        //    {
        //        switch (Info.Inventory[slot].Data.itemType)
        //        {
        //            //case ItemType.Tent: Owner.MyTent.OpenTent(); break;
        //            default:
        //                {
        //                    Owner.DataOut = SendType.Multi;
        //                    switch (tgrt)
        //                    {
        //                        case 0: for (int a = 0; a < 2; a++) Owner.MyStats.ItemUsage(Info.Inventory[slot].Data.statType[a], Info.Inventory[slot].Data.statVal[a]); break;
        //                        //default:for (int a = 0; a < 2; a++) 
        //                    }
        //                    Info.Inventory[slot].Ammt -= 1;
        //                    Send_9(slot, ammt);
        //                    Send_15();
        //                    Owner.DataOut = SendType.Normal;
        //                } break;
        //        }

        //    }

        //}
        //public void CancelItem(byte slot)
        //{
        //    //Get item first
        //    cInvItem i = Info.Inventory[slot];
        //    if (i.ID > 0)
        //    {
        //        switch (Info.Inventory[slot].Data.itemType)
        //        {
        //            //case ItemType.Tent: Owner.MyTent.CloseTent(); break;
        //        }

        //    }
        //}

        public void Send_Inventory() //user's primary inventory
        {
            SendPacket p = new SendPacket();
            p.Header(23, 5);
            p.AddArray(Info.Inventory._23_5Data);
            Send(p);
        }
        #endregion

        #region AccountInfo/Methods
        public UInt32 GetSlotID(int slot)
        {
            return myCharlist[slot].charID;
        }
        public void UpdateClientIM()
        {
            SendPacket im = new SendPacket();
            im.Header(35, 4);
            im.AddDWord(0/*cGlobal.WLO4EVERDB.GetIM(DbuserID)*/);
            im.AddDWord(0);
            im.AddDWord(0);
            im.AddDWord(0);
            Send(im);
        }
        #endregion

        #region CharacterInfo/Methods        
        public string CharacterName { get { if (Info != null) return Info.Name; else return ""; }}        
        public string Nickname { get { return Info.Nickname; }  }
        public MapPlugin CurrentMap { get { try { return Info.currentMap; } catch { return null; } } }
        public UInt16 X_Axis { get { if (Info != null)return Info.X; else return 0; } }
        public UInt16 Y_Axis { get { if (Info != null)return Info.Y; else return 0; } }

        public uint CharacterID
        {
            get
            {
                int add = 0;
                switch (UserLoc)
                {
                    case UserDBLoc.WLO4EVER: add += 2000; break;
                    case UserDBLoc.TEST: add += 1000; break;
                }
                if (Slot == 2) add += 4500000;

                return (uint)((int)DbuserID + add);
            }
        }
        
        public void UpdateClient_FriendList()
        {
            SendPacket y = new SendPacket();
            y.Header(14, 5);
            y.AddArray(new byte[]{100, 0, 0, 0, 6, 71, 77, 164, 164, 164, 223, 200, 0,      
        0, 0, 0, 0, 28, 175, 125, 26, 28, 175, 125, 26, 0, 0});
            foreach (Character h in Friends)
            {
                y.AddDWord(h.charID);
                y.AddString(h.Name);
                y.AddByte((byte)h.Level);
                y.AddByte(BitConverter.GetBytes(h.Reborn)[0]);
                y.AddByte((byte)h.Job);
                y.AddByte((byte)h.Element);
                y.AddByte((byte)h.Body);
                y.AddByte(h.Head);
                y.AddWord(h.HairColor);
                y.AddWord(h.SkinColor);
                y.AddWord(h.ClothingColor);
                y.AddWord(h.EyeColor);
                y.AddString(h.Nickname);
                y.AddByte(0);
            }
            Send(y);
        }

        public void Send_3_Me()
        {
            SendPacket p = new SendPacket();
            p.Header(3);
            p.AddDWord(CharacterID);
            p.AddByte((byte)Info.Body);
            p.AddWord(Info.loginMap);
            p.AddWord(X_Axis);
            p.AddWord(Y_Axis);
            p.AddByte(0); p.AddByte(Info.Head); p.AddByte(0);
            p.AddWord(Info.HairColor);
            p.AddWord(Info.SkinColor);
            p.AddWord(Info.ClothingColor);
            p.AddWord(Info.EyeColor);
            p.AddByte(Eqs.WornCount);//clothesAmmt); // ammt of clothes
            p.AddArray(Eqs.Worn_Equips);
            p.AddDWord(0);
            p.AddString(CharacterName);
            p.AddString(Nickname);
            p.AddDWord(0);
            Send(p);
        }
        public void Send_3_They(Player c)
        {
            SendPacket p = new SendPacket();
            p.Header(3);
            p.AddDWord(CharacterID);
            p.AddByte((byte)Info.Body);
            p.AddByte((byte)Info.Element);
            p.AddByte((byte)Info.Level);
            p.AddWord(Info.currentMap.MapID);
            p.AddWord(X_Axis);
            p.AddWord(Y_Axis);
            p.AddByte(0); p.AddByte(Info.Head); p.AddByte(0);
            p.AddWord(Info.HairColor);
            p.AddWord(Info.SkinColor);
            p.AddWord(Info.ClothingColor);
            p.AddWord(Info.EyeColor);
            p.AddByte(Eqs.WornCount);//clothesAmmt); // ammt of clothes
            p.AddArray(Eqs.Worn_Equips);
            p.AddDWord(0); p.AddByte(0);
            p.AddBool(Info.Reborn);
            p.AddByte((byte)Info.Job);
            p.AddString(CharacterName);
            p.AddString(Nickname);
            p.AddByte(255);
            c.Send(p);
        }

        public void Send_5_3() //logging in player info
        {
            SendPacket p = new SendPacket();
            p.Header(5, 3);
            p.AddByte((byte)Info.Element);
            p.AddDWord((uint)Info.CurHP);
            p.AddWord((ushort)Info.CurSP);
            p.AddWord(Info.Str); //base str
            p.AddWord(Info.Con); //base con
            p.AddWord(Info.Int); //base int
            p.AddWord(Info.Wis); //base wis
            p.AddWord(Info.Agi); //base agi
            p.AddByte((byte)Info.Level); //lvl
            p.AddDWord((uint)Info.TotalExp); //exp ???
            p.AddDWord((UInt32)(Info.Level - 1)); //lvl -1 ???
            p.AddDWord((uint)Info.FullHP); //max hp
            p.AddWord((ushort)Info.FullSP); //max sp

            //-------------- 7 DWords
            p.AddDWord(0);
            p.AddDWord(0);
            p.AddDWord(0);
            p.AddDWord(0);
            p.AddDWord(0);
            p.AddDWord(0);
            p.AddDWord(0);

            //--------------- Skills
            p.AddWord(0/*(ushort)MySkills.Count*/);
            //if (MySkills.Count > 0)
            //    p.AddArray(MySkills.GetSkillData());
            //p.AddWord(1); //ammt of skills
            //p.AddWord(188); p.AddWord(1); p.AddWord(0); p.AddByte(0); //skill data
            //--------------- table with rebirth and job
            p.AddWord(0); p.AddWord(0);
            p.AddByte(BitConverter.GetBytes(Info.Reborn)[0]); p.AddByte((byte)Info.Job); p.AddByte((byte)Info.Potential);

            Send(p);
        }

        #region stats
        public void SendGold()
        {
            SendPacket p = new SendPacket();
            p.Header(26, 4);
            p.AddDWord(Gold);
            Send(p);
        }
        
        public void FillHP()
        {
            Info.CurHP = Info.FullHP;
        }
        public void FillSP()
        {
            Info.CurSP = Info.FullSP;
        }
        #endregion

        #region equips

        public bool WearEQ(byte index)
        {
            lock (mylock)
            {
                bool ret = false;
                InvItemCell i = new InvItemCell();
                i.CopyFrom(Inv[index]);
                if (i.ItemID > 0)
                {
                    Inv[index].Clear();
                    if (Info.Level >= i.Data.Level)
                    {
                        DataOut = SendType.Multi;
                        var retrem = Eqs.SetEQ((byte)i.Data.EquipPos, i);
                        if (retrem != null && retrem.ItemID > 0)
                            Inv.AddItem(retrem, index, 1);
                        Eqs.Send8_1();//send ac8
                        SendPacket tmp = new SendPacket();
                        tmp.Header(5, 2);
                        tmp.AddDWord(CharacterID);
                        tmp.AddWord(i.ItemID);
                        CurrentMap.BroadcastEx(tmp, CharacterID);
                        tmp = new SendPacket();
                        tmp.Header(23, 17);
                        tmp.AddByte(index);
                        tmp.AddByte(index);
                        Send(tmp);
                        ret = true;
                        DataOut = SendType.Normal;
                    }
                }
           
            return ret;
            }
        }

        public bool unWearEQ(byte src, byte dst)
        {
            lock (mylock)
            {
                bool ret = false;
                InvItemCell i = null;
                if (Eqs[src].ItemID > 0)
                {
                    i = Eqs.RemoveEQ(src);//copy from clothes
                    if (i != null && Inv.AddItem(i, dst, 1) > 0)
                    {
                        DataOut = SendType.Multi;
                        SendPacket p = new SendPacket();
                        p.Header(23, 16);
                        p.AddByte(src);
                        p.AddByte(dst);
                        Send(p);
                        Eqs.Send8_1();
                        p = new SendPacket();
                        p.Header(5, 1);
                        p.AddDWord(CharacterID);
                        p.AddWord(i.ItemID);
                        CurrentMap.BroadcastEx(p, CharacterID);
                        ret = true;
                        DataOut = SendType.Normal;
                    }
                    else
                        Eqs.SetEQ(src, i);
                }

                return ret;
            }
        }

        #endregion
        #endregion

        #region Other Methods
        public void GenerateQueuepkt()
        {
            if (QueuePkt.Data.Length > 0)
            {
                SendPacket i = new SendPacket();
                i.AddPacket(QueuePkt);
                dataout = prevdataout;
                DatatoSend.Enqueue(i);
            }
        }

        void Send_9(uint ID, byte value)
        {
            SendPacket p = new SendPacket();
            p.Header(14, 9);
            p.AddDWord(ID);
            p.AddByte(value);
            Send(p);
        }
        #endregion

        #region WorldEvents/Other Events
        public void onPlayerLogin(Player src)
        {
            if(Friends.Exists(c=>c.charID == src.CharacterID))
                Send_9(src.CharacterID, 0);
        }

        void Event_onDataOut(SendPacket pkt)
        {
            Send(pkt);
        }
        #endregion

        #region Network Client Stuff    
        Queue<SendPacket> DatatoSend = new Queue<SendPacket>();
        SendPacket QueuePkt, MultiPkt;
        SendType dataout,prevdataout;
        public SendType DataOut
        {
            get { return dataout; }
            set
            {
                prevdataout = dataout; dataout = value;
                if (prevdataout == SendType.Multi && value == SendType.Normal) Send(MultiPkt);
                else if (value == SendType.Queue) QueuePkt = new SendPacket();
                else if (value == SendType.Multi) MultiPkt = new SendPacket();
            }
        }
        
        void Player_Connection_Terminated(object sender, EventArgs e)
        {
            if (Disconnected != null) Disconnected(this, null);
        }

        public override void Send(SendPacket sendpkt,int delay = 0)
        {
            switch (DataOut)
            {
                case SendType.Multi:MultiPkt.AddArray(sendpkt.Data);break;
                case SendType.Normal: base.Send(sendpkt); break;
            }
        }

        protected override void onDataRecieved(RecvPacket r)
        {
            if (RecievedData != null) RecievedData(r, this);
        }

        new public void Disconnect(bool sendevent)
        {
            if (sendevent)
                if (Disconnected != null) Disconnected(this, null);
            base.Disconnect();
        }

        #endregion

        void PropChng(string txt)
        {
            if(PropertyChanged != null)PropertyChanged(this,new System.ComponentModel.PropertyChangedEventArgs(txt));
        }

    }
}
