﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace pserver
{
    public class Mail
    {
        public uint id;
        public string message;
        public string type;
        public uint targetid;
        public void Load(string r)
        {
            var words = r.Split(' ');
            id = (UInt16)(Int64.Parse(words[0]));
            type = (words[2]);
            targetid = (UInt16)(Int64.Parse(words[3]));
            message = words[1];
        }

    }
}
