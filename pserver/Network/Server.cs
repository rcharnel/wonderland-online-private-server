﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace pserver.Network
{
    public class Server
    {
        List<Player> Clients = new List<Player>();
        
        public void WlOTcpListener_ClientConnected(System.Net.Sockets.Socket rawSock)
        {
            Player player = new Player();
            player.Connect(ref rawSock);
            player.Disconnected += player_Disconnected;
            player.RecievedData += player_RecievedData;
            Clients.Add(player);
            DLogger.SystemLog(rawSock.RemoteEndPoint.ToString() + "  has connected");
        }

        void player_RecievedData(Network.RecvPacket pkt, Player sender)
        {
            DLogger.NetworkLog(sender.ClientIP,sender.CharacterName, pkt.Data);
            cGlobal.PacketList.Enqueue(new WloPacket() { src = sender, packet = pkt });
        }

        void player_Disconnected(object sender, EventArgs e)
        {
            if ((sender as Player).Info != null)
            {
                cGlobal.CharacterDB.unLockName((sender as Player).CharacterName);
                SendPacket bye = new SendPacket();
                bye.Header(1, 1);
                bye.AddDWord((sender as Player).CharacterID);
               cGlobal.World.BroadcastEx(bye, (sender as Player).CharacterID);
                if (!cGlobal.DEVMODE)
                {
                    if (cGlobal.CharacterDB.WritePlayer((sender as Player).CharacterID, (sender as Player).DBData(), (byte)(sender as Player).Info.Potential, (sender as Player).Info.Stat_toSave,
                        null,(sender as Player).Eqs.EqData))
                    {
                        try
                        {
                            var id = (sender as Player).CharacterID;
                            DLogger.SystemLog((sender as Player).userName + " Info has been Fully Saved");
                        }
                        catch (Exception r) { DLogger.ErrorLog(r.StackTrace, r.Message); }
                    }
                }
            }
            if (Clients.Exists(c => c == sender as Player)) Clients.Remove(sender as Player);
            DLogger.SystemLog((sender as Player).ClientIP + " has Disconnected");

        }

        public void ShutDown()
        {
            Clients.ToList().ForEach(new Action<Player>(c => c.Disconnect(true)));
        }
    }
}
