﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.Net.Sockets;

namespace pserver.Network
{
    public abstract class NetClient
    {
        object mylock = new object();
        IAsyncResult connstate, recvstate;
        int holdSize;
        byte[] holdData;
        protected event EventHandler Connection_Terminated;
        public byte[] buffer;
        Socket client;
        public int RecvTimeout = 30000;
        public int SendTimeout = 30000;
        AddressFamily addressfamily;
        SocketType sockettype;
        ProtocolType protocoltype;

        public NetClient()
        {
            this.addressfamily = AddressFamily.InterNetwork; ; this.protocoltype = ProtocolType.Tcp; this.sockettype = SocketType.Stream;
            buffer = new byte[20024];
        }

        public void Dispose()
        {
            if (client != null)
                client.Dispose();
            buffer = null;

        }
        public string ClientIP
        {
            get { try { return ((IPEndPoint)client.RemoteEndPoint).Address.ToString(); } catch { return "0.0.0.0"; } }
        }

        [System.ComponentModel.Browsable(false)]
        public bool isConnected
        {
            get
            {
                try
                {
                    if (client == null) return false;
                    
                    if (client.GetSocketOption(SocketOptionLevel.Socket, SocketOptionName.KeepAlive, 1)[0].Equals(1))
                        client.BeginReceive(new byte[1], 0, 1, SocketFlags.None, null, null);

                        if (client.Connected == false)
                            return false;

                    
                    bool bSelectRead = client.Poll(1, SelectMode.SelectRead);

                    bool bSelectWrite = client.Poll(1, SelectMode.SelectWrite);

                    int available = client.Available;

                    //if (bSelectWrite && bSelectRead && available 0)

                    return !((available == 0 ) & bSelectRead);
                    //{
                    //    //return true;

                    //    //checkSocket.BeginReceive(new byte[1], 0, 1, SocketFlags.Peek, null, null);

                    //    client.Receive(new byte[0], 0, 0, SocketFlags.Peek);

                    //    client.Send(new byte[0], 0, 0, SocketFlags.None);

                    //    return client.Connected;

                    //}

                    //else

                    //    return false;

                }

                catch (SocketException)
                {

                    return false;

                }

                catch (ObjectDisposedException)
                {

                    return false;

                }
                catch (Exception e)
                {
                    return false;
                }

                //try
                //{
                //    bool part1 = client.Poll(1000, SelectMode.SelectRead);
                //    bool part2 = (client.Available == 0);
                //    return !(part1 & part2);
                //}
                //catch { return false; }
            }
        }

        /// <summary>
        /// Establish a connection to a remote location using default tcp ,and stream type socket
        /// </summary>
        /// <param name="IP">ip address ex is 192.168.1.1</param>
        /// <param name="port">port number to connect to.</param>
        /// <param name="blocking_mode">Set Socket to blocking or non blocking</param>
        public void Connect(ref Socket client)
        {
            this.client = client;
            client.Blocking = false;
            buffer = new byte[client.Available];
            try
            {
                recvstate = client.BeginReceive(buffer, 0, client.Available, SocketFlags.None, DataRecieved, null);
            }
            catch (ObjectDisposedException e)
            {
            }
            catch (Exception r) { DLogger.ErrorLog(r.StackTrace, r.Message); }
        }

        void Encode(ref byte[] data)
        {
            for (int n = 0; n < data.Length; n++)
            {
                data[n] = (byte)(173 ^ data[n]);
            }
        }

        
        void DataRecieved(IAsyncResult iar)
        {
            try
            {
                int nRef = client.EndReceive(iar);

                if (client != null)
                {
                    if (isConnected && nRef == 0)// Disconnection
                    {
                        int len = client.Available;
                        buffer = new byte[len];
                        recvstate = client.BeginReceive(buffer, 0, len, SocketFlags.None, DataRecieved, null);
                    }
                    else if (isConnected && nRef != 0)
                    {
                        byte[] data = null;
                        int len = buffer.Length;
                        int origLen = len;

                        Encode(ref buffer);
                        //check for leftover data
                        if (holdSize > 0)
                        {
                            //formPtr.Log("Leftover Found");
                            //formPtr.Log(("last 2 byte Data: " + holdData[holdSize - 2] + " " + holdData[holdSize - 1]));
                            len += holdSize;
                            data = new byte[len];
                            holdData.CopyTo(data, 0);
                            buffer.CopyTo(data, holdSize);
                            holdSize = 0;
                            //p.holdData = null;
                        }
                        else
                        {
                            data = new byte[len]; buffer.CopyTo(data, 0);
                        }

                        int at = 0;
                        while (at < (len - 4))
                        {
                            if ((data[at] == 244) && (data[at + 1] == 68))
                            {
                                int size = data[at + 2] + (data[at + 3] << 8);
                                if ((size + 4) > (len - at)) //not all packet present, so store remainder
                                {
                                    holdSize = len - at;
                                    holdData = new byte[holdSize];
                                    Array.Copy(data, at, holdData, 0, holdSize);
                                    at = len;
                                }
                                else //full packet should be in this block
                                {
                                    byte[] packetData = new byte[size];
                                    Array.Copy(data, at + 4, packetData, 0, size);
                                    //send temppacket onto packet list
                                    var yy = new RecvPacket();
                                    yy.AddArray(packetData);
                                    onDataRecieved(yy);

                                    //move ptr
                                    at += size + 4;
                                }

                            }
                            else
                                at++;
                        }

                        if (at < len) //we have leftover data too small to be a header, put on hold
                        {
                            holdSize = len - at;
                            holdData = new byte[holdSize];
                            Array.Copy(data, at, holdData, 0, holdSize);
                            at = len;
                            //formPtr.Log(("last 2 byte Data: "+ holdData[holdSize-2]+" "+holdData[holdSize-1]));
                        }

                        len = client.Available;
                        buffer = new byte[len];
                        recvstate = client.BeginReceive(buffer, 0, len, SocketFlags.None, DataRecieved, null);

                    }
                    else
                    throw new SocketException(10057);
                }
                

            }
            catch (SocketException ex)
            {
                if (ex.ErrorCode == 10057)
                {
                    if (Connection_Terminated != null)
                        Connection_Terminated(this, null);
                    Disconnect();
                }
                else if (ex.ErrorCode != (int)SocketError.WouldBlock)
                {
                    DLogger.ErrorLog(ex.StackTrace, ex.Message);
                    if (Connection_Terminated != null)
                        Connection_Terminated(this, null);
                    Disconnect();

                }
            }
            catch (Exception r)
            {
                DLogger.ErrorLog(r.StackTrace, r.Message);
                if (Connection_Terminated != null)
                    Connection_Terminated(this, null);
                Disconnect();
            }
            
        }

        protected abstract void onDataRecieved(RecvPacket r);

        public virtual void Send(SendPacket data,int delay = 0)
        {
            Send(data.Data,delay);
        }
        public virtual void Send(byte[] data,int delay = 0)
        {
            lock (mylock)
            {
                try
                {
                    Encode(ref data);
                    client.Send(data);
                    if (delay > 0)
                        System.Threading.Thread.Sleep(delay);
                }
                catch (Exception e)
                {
                }
            }
        }


        public virtual void Disconnect()
        {

            try
            {
                client.Shutdown(SocketShutdown.Both);
                client.Close();
                client = null;
            }
            catch (Exception r) { }
        }


    }
}
