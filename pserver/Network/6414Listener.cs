﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.Net.Sockets;

namespace pserver.Network
{
    public class _6414Listener
    {
        TcpListener listener;
        public bool isOnline;
        int port = 6414;

        //When we've received a User we send an event
        public event onNetClientConnect ClientConnected;

        public _6414Listener()
        {
        }

        public void AcceptClient(IAsyncResult ar)
        {
            try
            {
                if (ClientConnected != null) ClientConnected(this.listener.EndAcceptSocket(ar));
                else
                    (this.listener.EndAcceptSocket(ar)).Disconnect(false);
                
                this.ListenForNewClient();
            }
            catch(ObjectDisposedException y) { }
            catch (Exception r) { DLogger.ErrorLog(r.StackTrace, r.Message); }
        }

        public void Start()
        {
            try
            {
                listener = new TcpListener(IPAddress.Any, port);
                //Same as the kickstart
                DLogger.SystemLog("Starting TcpListener");
                this.listener.Start();
                this.ListenForNewClient();
                DLogger.SystemLog("Listening on " + listener.LocalEndpoint);
                isOnline = true;
            }
            catch (ObjectDisposedException r) { }
            catch (Exception r) { DLogger.ErrorLog(r.StackTrace, r.Message); }
        }

        public void ListenForNewClient()
        {
            //Just as for the Read/BeginRead-methods in the TcpClient the BeginAcceptTcpClient
            //is the AcceptTcpClient()s asyncrounous brother.
            try
            {
                this.listener.BeginAcceptTcpClient(this.AcceptClient, null);
            }
            catch (ObjectDisposedException y) { }
            catch (Exception r) { DLogger.ErrorLog(r.StackTrace, r.Message); }
        }

        public void Stop()
        {
            this.listener.Stop();
            isOnline = false;
        }
    }
}
