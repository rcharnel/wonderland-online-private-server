﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace pserver.Network
{
    /// <summary>
    /// WLO Recieve Packet Class
    /// </summary>
    public class RecvPacket : EventArgs
    {
        public byte expectedsize;
        List<byte> data;
        public byte a { get { return data[0]; } }
        public byte b { get { if (data.Count > 1)return data[1]; else return 0; } }
        public byte[] Data { get { return data.ToArray(); } }
        public int Size { get { return data.Count; } }
        public RecvPacket()
        {
            data = new List<byte>();
        }
        public bool GetBool(int at)
        {
            return BitConverter.ToBoolean(Data, at);
        }
        public byte GetByte(int at)
        {
            return data[at];
        }
        public UInt16 GetWord(int at)
        {
            UInt16 v = (UInt16)(data[at] + (data[at + 1] << 8));
            return v;
        }
        public UInt32 GetDWord(int at)
        {
            UInt32 v = (UInt32)(data[at] + (data[at + 1] << 8) + (data[at + 2] << 16) + (data[at + 3] << 24));
            return v;
        }
        public UInt64 GetLong(int at)
        {
            UInt64 v = BitConverter.ToUInt64(data.ToArray(), at);
            return v;
        }
        public string GetString(int at)
        {
            try
            {
                int len = data[at];
                char[] c = new char[len];
                Array.Copy(data.ToArray(), at + 1, c, 0, len);
                string str = new string(c);
                return str;
            }
            catch { }
            return "";
        }
        public string GetStringRaw(int at, int len)
        {
            try
            {
                char[] c = new char[len];
                Array.Copy(data.ToArray(), at, c, 0, len);
                string str = new string(c);
                return str;
            }
            catch { }
            return "";
        }
        public void AddArray(byte[] a)
        {
            if (a != null)
            {
                for (int n = 0; n < a.Length; n++)
                    data.Add((byte)a[n]);
            }
        }
    }
    /// <summary>
    /// WLO Send Packet Class
    /// </summary>
    public class SendPacket
    {
        bool hasHeader = false;
        List<byte> data;public byte[] Data { get { return data.ToArray(); } }
        public int PacketSize { get { return data.Count; } }

        public SendPacket()
        {
            Clear();

        }
        void Clear()
        {
            data = new List<byte>(); hasHeader = false;
        }

        public void Header(byte ac, byte subac)
        {
            if (data.Count != 0) Clear();
            data.AddRange(new byte[] { 244, 68, 0, 0 });
            data.Add((byte)ac);
            data.Add((byte)subac);
            data[2] = (byte)(PacketSize - 4);
            data[3] = (byte)((PacketSize - 4) >> 8);
            hasHeader = true;
        }
        public void Header(byte ac)
        {
            if (data.Count != 0) Clear();
            data.AddRange(new byte[] { 244, 68, 0, 0 });
            data.Add((byte)ac);
            data[2] = (byte)(PacketSize - 4);
            data[3] = (byte)((PacketSize - 4) >> 8);
            hasHeader = true;
        }
                
        public void AddByte(byte v)
        {
            data.Add(v);
            if (hasHeader)
            {
                data[2] = (byte)(PacketSize - 4);
                data[3] = (byte)((PacketSize - 4) >> 8);
            }
        }
        public void AddBool(bool v)
        {
            data.Add(BitConverter.GetBytes(v)[0]);
            if (hasHeader)
            {
                data[2] = (byte)(PacketSize - 4);
                data[3] = (byte)((PacketSize - 4) >> 8);
            }
        }
        public void AddWord(UInt16 v)
        {
            data.Add((byte)v);
            data.Add((byte)(v >> 8));
            if (hasHeader)
            {
                data[2] = (byte)(PacketSize - 4);
                data[3] = (byte)((PacketSize - 4) >> 8);
            }
        }
        public void AddDWord(UInt32 v)
        {
            data.Add((byte)v);
            data.Add((byte)(v >> 8));
            data.Add((byte)(v >> 16));
            data.Add((byte)(v >> 24));
            if (hasHeader)
            {
                data[2] = (byte)(PacketSize - 4);
                data[3] = (byte)((PacketSize - 4) >> 8);
            }
        }
        public void AddDouble(double v)
        {
            var e = BitConverter.GetBytes(v);
            data.AddRange(e);
            if (hasHeader)
            {
                data[2] = (byte)(PacketSize - 4);
                data[3] = (byte)((PacketSize - 4) >> 8);
            }
        }
        public void AddLong(long v)
        {
            var e = BitConverter.GetBytes(v);
            data.AddRange(e);
            if (hasHeader)
            {
                data[2] = (byte)(PacketSize - 4);
                data[3] = (byte)((PacketSize - 4) >> 8);
            }
        }
        public void AddString(string str, bool withlength = true)
        {
            if (withlength)
                data.Add((byte)str.Length);
            for (int n = 0; n < str.Length; n++)
                data.Add((byte)str[n]);
            if (hasHeader)
            {
                data[2] = (byte)(PacketSize - 4);
                data[3] = (byte)((PacketSize - 4) >> 8);
            }
        }
        public void AddPacket(SendPacket src)
        {
            if (src != null)
            {
                for (int n = 0; n < src.PacketSize; n++)
                    data.Add((byte)src.Data[n]);
            }
        }
        public void AddArray(byte[] a)
        {
            if (a != null)
            {
                for (int n = 0; n < a.Length; n++)
                    data.Add((byte)a[n]);
                if (hasHeader)
                {
                    data[2] = (byte)(PacketSize - 4);
                    data[3] = (byte)((PacketSize - 4) >> 8);
                }
            }
        }
    }
}
