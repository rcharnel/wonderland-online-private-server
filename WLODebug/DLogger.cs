﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace pserver
{
    public enum LogType
    {
        System,
        DataBase,
        Error,
        NetWork,
    }

    public struct DItem:INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;
        DateTime when;
        LogType type;
        string message;
        public string where;
        public DateTime When { get { return when; } set { when = value; if (PropertyChanged != null)PropertyChanged(this, new PropertyChangedEventArgs("When")); } }
        public LogType Type { get { return type; } set { type = value; if (PropertyChanged != null)PropertyChanged(this, new PropertyChangedEventArgs("Type")); } }
        public string Message { get { return message; } set { message = value; if (PropertyChanged != null)PropertyChanged(this, new PropertyChangedEventArgs("Message")); } }
    }

    /// <summary>
    /// Class Used to Log Informaton from the server
    /// </summary>
    public static class DLogger
    {
        public static Form FrmSrc;
        static BindingList<DItem> logList = new BindingList<DItem>(); public static BindingList<DItem> LogList { get { return logList; } }

        public static void SystemLog(string txt)
        {
            try{FrmSrc.Invoke(new Action(() => { logList.Add(new DItem() { When = DateTime.Now,where ="", Type = LogType.System, Message = txt }); }));  }catch{}
        }
        public static void DataBaseLog(string txt)
        {
            try{ FrmSrc.Invoke(new Action(() => { logList.Add(new DItem() { When = DateTime.Now, where ="", Type = LogType.DataBase, Message = txt }); }));  }catch{}
        }

        public static void NetworkLog(string ip,string player, byte[] data)
        {
             try{FrmSrc.Invoke(new Action(() => { logList.Add(new DItem() { When = DateTime.Now, where = "", Type = LogType.NetWork, Message = string.Format("Packet Data ( {0} ) from {1}", string.Join(",", data), ip + " " + player) }); }));  }catch{}
        }
        public static void NetworkLog(object a, object b, byte[] data)
        {
            try{ FrmSrc.Invoke(new Action(() => { logList.Add(new DItem() { When = DateTime.Now, where = "", Type = LogType.NetWork, Message = string.Format("Ac {0} SubAC {1} : Data ( {2} )", a, b, string.Join(",", data)) }); }));  }catch{}
        }
        public static void NetworkLog(object a, object b, string txt)
        {
            try { FrmSrc.Invoke(new Action(() => { logList.Add(new DItem() { When = DateTime.Now, where = "", Type = LogType.NetWork, Message = string.Format("Ac {0} SubAC {1} {2}", a, b, txt) }); })); }
            catch { }
        }

        public static void ErrorLog(string whereat,string txt)
        {
            try{ FrmSrc.Invoke(new Action(() => { logList.Add(new DItem() { When = DateTime.Now,where = "at "+whereat,  Type = LogType.Error, Message = txt }); })); }catch{}
        }
        public static DItem PullFirstLog() { DItem s = logList[0]; FrmSrc.Invoke(new Action(() => { logList.RemoveAt(0); })); return s; }
    }
}
